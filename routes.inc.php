<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:30
 */
return array(
	'admin/(.+)' => 'admin/list/$1',
	'index' => 'index/index',
	'([^/]+)/([^/]+)/([^/]+)' => '$1/$2/$3',
	'([^/]+)/([^/]+)' => '$1/$2',
	'([^/]+)' => '$1',
);