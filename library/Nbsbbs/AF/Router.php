<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:27
 */
namespace Nbsbbs\AF;

class Router {
	// Хранит конфигурацию маршрутов.
	private $routes, $registry, $defaultActions = array("index", "list");

	function __construct() {
		$this->registry = Registry::getInstance();
		$this->baseUrl = $this->registry->config->baseUrl;
		// Получаем конфигурацию из файла.
		$this->routes = include($this->registry->routesPath);
	}

	// Метод получает URI. Несколько вариантов представлены для надёжности.
	function getURI() {
		if (!empty($_SERVER['REQUEST_URI'])) {
			return trim($_SERVER['REQUEST_URI'], '/');
		}

		if (!empty($_SERVER['PATH_INFO'])) {
			return trim($_SERVER['PATH_INFO'], '/');
		}

		if (!empty($_SERVER['QUERY_STRING'])) {
			return trim($_SERVER['QUERY_STRING'], '/');
		}
	}
	
	function checkAuth() {
		$authOk = false; 
		if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
			if (($_SERVER['PHP_AUTH_USER']) == ADMIN_USER and ($_SERVER['PHP_AUTH_PW'] == ADMIN_PASS)) {
				$authOk = true;
			}
		}
		if (!$authOk) {
			return false;
		} else {
			return true;
		}
	}
	
	function requestAuth() {
		header('WWW-Authenticate: Basic realm="Admin Page"');
		header('HTTP/1.0 401 Unauthorized');
		echo 'Сюда кого попало не пускают.';
		die();
	}

	function run() {
		$isCalled = false;
		// Получаем URI.
		$uri = $this->getURI();
		// cut base path
		if ($this->baseUrl != "") {
			$uri = preg_replace("~^" . trim($this->baseUrl, "/") . "~", "", $uri);
		}
		$uri = trim($uri, "/");
		// Пытаемся применить к нему правила из конфигурации.
		foreach ($this->routes as $pattern => $route) {
			// Если правило совпало.
			if (preg_match("~$pattern~", $uri)) {
				// Получаем внутренний путь из внешнего согласно правилу.
				$internalRoute = preg_replace("~$pattern~", $route, $uri);
				// Разбиваем внутренний путь на сегменты.
				$segments = explode('/', $internalRoute);

				// Первый сегмент — контроллер.
				$controller = __NAMESPACE__ . '\Controller_' . ucfirst(array_shift($segments));
				// Второй — действие.
				$action = 'action' . ucfirst(array_shift($segments));
				// Остальные сегменты — параметры.
				$parameters = $segments;

				// Если не загружен нужный класс контроллера или в нём нет
				// нужного метода — вызываем Controller_Error->actionError;
				if (!empty($controller) and ($action == "action")) {
					foreach ($this->defaultActions as $defAction) {
						$defAction = "action" . ucfirst($defAction);
						if (is_callable(array($controller, $defAction))) {
							$action = $defAction;
							break;
						}
					}
				}
				
				if (!in_array($controller, array("Nbsbbs\AF\Controller_Api"))) {
					if (!$this->checkAuth()) {
						$this->requestAuth();
					}
				}

				if (!is_callable(array($controller, $action))) {
					$parameters = array("Поведение действия {$action} контроллера " . $controller . " не описано");
					$controller = __NAMESPACE__ . "\Controller_Error";
					$controllerObject = new $controller($this->registry);
					$action = "actionError";
					call_user_func_array(array($controllerObject, $action), array($parameters));
					$isCalled = true;
					break;
				} else {
					// Вызываем действие контроллера с параметрами
					$controllerObject = new $controller();
					call_user_func_array(array($controllerObject, $action), $parameters);
					$isCalled = true;
					break;
				}
			}
		}
		if (!$isCalled) {
			if (!in_array($controller, array("Controller_Api"))) {
				if (!$this->checkAuth()) {
					$this->requestAuth();
				}
			}
			
			$controllerObject = new Controller_Index($this->registry);
			call_user_func_array(array($controllerObject, "actionIndex"), array());
		}

		return;
	}
}