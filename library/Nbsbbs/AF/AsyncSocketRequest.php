<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 14:36
 */
namespace Nbsbbs\AF;

class AsyncSocketRequest {
	private $timeout = 5;
	private $useragent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.202 Safari/535.1";
	private $cookies = array();
	private $lastHeaders = array();
	private $isTimeout = false;

	/**
	 * @param string $url
	 * @return bool|string
	 */
	public function get($url) {
		$cache = new \Nbsbbs\Cache\Cache_Memcache_Local();
		if ($text = $cache->get(md5($url))) {
			return $text;
		}
		$this->isTimeout = false;
		$urlinfo = parse_url($url);
		$startTime = microtime(true);
		if ($file = stream_socket_client('tcp://' . $urlinfo['host'] . ':80', $errno, $errstr, $this->timeout)) {
			stream_set_blocking($file, 0);
			if (!empty($urlinfo['query'])) {
				fwrite($file, "GET " . $urlinfo['path'] . "?" . $urlinfo['query'] . " HTTP/1.0\r\n");
			} else {
				fwrite($file, "GET " . $urlinfo['path'] . " HTTP/1.0\r\n");
			}
			fwrite($file, "Host: " . $urlinfo['host'] . "\r\n");
			fwrite($file, "Accept: */*\r\n");
			if (!empty($this->useragent)) fwrite($file, "User-agent: " . $this->useragent . "\r\n");
			if (!empty($this->cookies)) fwrite($file, "Cookie: " . $this->createCookieString($this->cookies) . "\r\n");
			fwrite($file, "\r\n");
			$headerFound = false;
			$tmp = "";
			do {
				if ($tmpp = fread($file, 4096)) {
					$tmp .= $tmpp;
				} else {
					usleep(50000);
				}
				if (microtime(true) - $startTime > $this->timeout) {
					$this->isTimeout = true;
					return false;
				}
			} while (!feof($file));
			if (!$headerFound and (strpos($tmp, "\r\n\r\n") > 0)) {
				$headerFound = true;
				$header = substr($tmp, 0, strpos($tmp, "\r\n\r\n"));
				$header = explode("\r\n", $header);
				$this->lastHeaders = $header;
				$tmp = substr($tmp, strpos($tmp, "\r\n\r\n") + 4);
			}
			$cache->set(md5($url), $tmp, 24 * 3600);
			return $tmp;
		} else {
			return false;
		}
	}

	/**
	 * @param string $url URL
	 * @param array $data Данные POST
	 * @return bool|string
	 */
	public function post($url, $data) {
		$this->isTimeout = false;
		$datastr = http_build_query($data);
		$urlinfo = parse_url($url);
		$startTime = microtime(true);
		if ($file = stream_socket_client('tcp://' . $urlinfo['host'] . ':80', $errno, $errstr, $this->timeout)) {

			if (!empty($urlinfo['query'])) {
				fwrite($file, "POST " . $urlinfo['path'] . "?" . $urlinfo['query'] . " HTTP/1.1\r\n");
			} else {
				fwrite($file, "POST " . $urlinfo['path'] . " HTTP/1.1\r\n");
			}
			fwrite($file, "Host: " . $urlinfo['host'] . "\r\n");
			fwrite($file, "Accept: */*\r\n");
			if (!empty($this->useragent)) fwrite($file, "User-agent: " . $this->useragent . "\r\n");
			if (!empty($this->cookies)) fwrite($file, "Cookie: " . $this->createCookieString($this->cookies) . "\r\n");
			fwrite($file, "Connection: close\r\n");
			fwrite($file, "Content-type: application/x-www-form-urlencoded\r\n");
			fwrite($file, "Content-length: " . strlen($datastr) . "\r\n");
			fwrite($file, "\r\n");
			fwrite($file, $datastr . "\r\n");
			fwrite($file, "\r\n");
			stream_set_blocking($file, 0);
			$headerFound = false;
			$tmp = "";
			do {
				if ($tmpp = fread($file, 4096)) {
					$tmp .= $tmpp;
				} else {
					usleep(50000);
				}
				if (microtime(true) - $startTime > $this->timeout) {
					$this->isTimeout = true;
					return false;
				}
			} while (!feof($file));
			if (!$headerFound and (strpos($tmp, "\r\n\r\n") > 0)) {
				$headerFound = true;
				$header = substr($tmp, 0, strpos($tmp, "\r\n\r\n"));
				$header = explode("\r\n", $header);
				$this->lastHeaders = $header;
				$tmp = substr($tmp, strpos($tmp, "\r\n\r\n") + 4);

			}
			return $tmp;
		} else {
			return false;
		}
	}

	public function createCookieString($cookies) {
		$collection = array();
		foreach ($cookies as $name => $value) {
			$collection[] = rawurlencode($name) . "=" . rawurlencode($value);
		}
		return implode("; ", $collection);
	}

	public function getHeaders() {
		return $this->lastHeaders;
	}

	public function setTimeout($timeout) {
		$this->timeout = $timeout;
	}

	public function setUserAgent($agent) {
		$this->useragent = $agent;
	}

	public function setCookies($cookies) {
		$this->cookies = $cookies;
	}

	public function isTimeout() {
		return $this->isTimeout;
	}
}