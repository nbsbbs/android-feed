<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 02.03.13
 * Time: 21:23
 */
namespace Nbsbbs\AF;

abstract class Form_Field_Constraint {
	public $param;
	public $messages = array();

	/**
	 * @return string[] Массив ошибок
	 */
	abstract public function getMessages();

	public function __construct($param = null) {
		if (!empty($param)) {
			$this->param = $param;
		}
	}

	/**
	 * @param $value
	 * @return bool
	 */
	abstract public function check($value);
}