<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:42
 */
namespace Nbsbbs\AF;

class Form_Field_String extends Form_Field {

	public function render($type = "form") {
		if (!empty($this->customDecorator) and is_callable($this->customDecorator)) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			if ($type == "form") {
				return "<label>" . $this->displayName . "</label>\n<input type='text' name='" . htmlspecialchars($this->fieldName, null, "UTF-8") . "' value='" . htmlspecialchars($this->currentValue, null, "UTF-8") . "'>\n<span class='help-block'>" . $this->inlineHelp . "</span>";
			} elseif ($type == "list") {
				return htmlspecialchars($this->currentValue);
			}
		}
	}


}