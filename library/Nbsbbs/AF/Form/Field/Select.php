<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 05.03.13
 * Time: 19:30
 */
namespace Nbsbbs\AF;

class Form_Field_Select extends Form_Field {
	/**
	 * @var array index => value
	 */
	public $possibleValues = array();

	/**
	 * @var string Название таблицы для выборки
	 */
	public $tableName;
	/**
	 * @var string Индекс поля для $fieldName
	 */
	public $indexField;
	/**
	 * @var string Индекс поля для $displayName
	 */
	public $displayField;
	/**
	 * @var string SQL-код дополнительного фильтра
	 */
	public $filter = "";

	public function setSourceTable($tableName) {
		$this->tableName = $tableName;
	}

	public function setFilter($filter) {
		$this->filter = $filter;
	}

	/**
	 * Задать индексы полей для отображения
	 *
	 * @param string $index Поле индекса
	 * @param string $display Поле отображения
	 */
	public function setFieldsIndex($index, $display) {
		$this->indexField = $index;
		$this->displayField = $display;
	}

	/**
	 * Заполняет поле $possibleValues
	 * @throws \Exception
	 */
	public function getPossibleValues() {
		if (empty($this->tableName)) throw new \Exception("Table name not set");
		if (empty($this->indexField)) throw new \Exception("Index field name not set");
		if (empty($this->displayField)) throw new \Exception("Display field name not set");
		if (!empty($this->filter)) {
			$filter = "AND " . $this->filter;
		} else {
			$filter = "";
		}
		$stmt = Registry::getInstance()->db->prepare("SELECT `" . $this->indexField . "`, `" . $this->displayField . "` FROM `" . $this->tableName . "` WHERE 1 {$filter} order by `" . $this->displayField . "`");
		$stmt->execute();
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			$this->possibleValues[$row[$this->indexField]] = $row[$this->displayField];
		}
	}

	/**
	 * Возвращает отображаемое значение, принимает хранимое
	 *
	 * @param $index
	 * @return string
	 * @throws \Exception
	 */
	public function getDisplayValueByIndex($index) {
		if (empty($this->tableName)) throw new \Exception("Table name not set");
		if (empty($this->indexField)) throw new \Exception("Index field name not set");
		if (empty($this->displayField)) throw new \Exception("Display field name not set");
		$stmt = Registry::getInstance()->db->prepare("SELECT `" . $this->displayField . "` FROM `" . $this->tableName . "` where `" . $this->indexField . "`=?");
		$stmt->execute(array($index));
		if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
			return $row[0];
		} else {
			return $index;
		}
	}

	public function render($type = "form") {
		if (!empty($this->customDecorator) and is_callable($this->customDecorator)) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			if ($type == "form") {
				$this->getPossibleValues();
				ob_start();
				?>
            <label><?=htmlspecialchars($this->displayName, null, "UTF-8")?></label>
            <select name="<?=htmlspecialchars($this->fieldName, null, "UTF-8")?>">
				<?
				foreach ($this->possibleValues as $valIndex => $valDisplay) {
					?>
                    <option value="<?=htmlspecialchars($valIndex, null, "UTF-8")?>"<?=($valIndex == $this->currentValue) ? (" selected") : ("")?>><?=htmlspecialchars($valDisplay, null, "UTF-8")?></option>
					<?
				}
				?>
            </select>
            <span class="help-block"><?=$this->inlineHelp?></span>

			<?
				$text = ob_get_clean();
				return $text;
			} elseif ($type == "list") {
				return htmlspecialchars($this->getDisplayValueByIndex($this->currentValue));
			}
		}
	}
}