<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 21:44
 */
namespace Nbsbbs\AF;

class Form_Field_Constraint_ValidId extends Form_Field_Constraint {

	/**
	 * @param string $param Название таблицы
	 */
	public function __construct($param) {
		$this->param = $param;
	}

	/**
	 * @return string[] Массив ошибок
	 */
	public function getMessages() {
		return $this->messages;
	}

	/**
	 * @param $value
	 * @return bool
	 */
	public function check($value) {
		$stmt = Registry::getInstance()->db->prepare("SELECT count(*) FROM " . $this->param . " WHERE id=?");
		$stmt->execute(array($value));
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			if ($row['count(*)'] > 0) {
				return true;
			} else {
				$this->messages[] = "Таблица " . $this->param . " не содержит нужного айди " . $value;
				return false;
			}
		}
		$this->messages[] = "Не получилось выполнить запрос к таблице " . $this->param;
		return false;
	}
}