<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 21:41
 */
namespace Nbsbbs\AF;

class Form_Field_Constraint_NotEmpty extends Form_Field_Constraint {

	/**
	 * @return string[] Массив ошибок
	 */
	public function getMessages() {
		return $this->messages;
	}

	/**
	 * @param $value
	 * @return bool
	 */
	public function check($value) {
		if (empty($value)) {
			$this->messages[] = "Пустое значение поля не допускается";
			return false;
		} else {
			return true;
		}
	}
}