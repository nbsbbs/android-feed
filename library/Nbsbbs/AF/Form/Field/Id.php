<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:38
 */
namespace Nbsbbs\AF;

class Form_Field_Id extends Form_Field {

	public function __construct() {
		$this->fieldName = "id";
	}

	public function render($type = "form") {
		if (!empty($this->customDecorator) and is_callable($this->customDecorator)) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			if ($type == "form") {
				return "<input type='hidden' name='id' value='" . htmlspecialchars($this->currentValue, null, "UTF-8") . "'>";
			} elseif ($type == "list") {
				return $this->currentValue;
			}
		}
	}
}