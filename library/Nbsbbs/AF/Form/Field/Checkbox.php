<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 15:03
 */
namespace Nbsbbs\AF;

class Form_Field_Checkbox extends Form_Field {

	public function convertValueToDbFormat($value) {
		if (!empty($value)) {
			return 1;
		} else  return 0;
	}

	public function render($type = "form") {
		if (!empty($this->customDecorator) and is_callable($this->customDecorator)) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			if ($type == "form") {
				return "<label class='checkbox'>" . $this->displayName . "\n<input type='checkbox' name='" . htmlspecialchars($this->fieldName, null, "UTF-8") . "'" . ((!empty($this->currentValue)) ? (" checked") : ("")) . "><span class='help-block'>" . $this->inlineHelp . "</span></label>";
			} elseif ($type == "list") {
				return ((!empty($this->currentValue)) ? ("Да") : ("Нет"));
			}
		}
	}
}