<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:56
 */
namespace Nbsbbs\AF;

class Form_Field_Timestamp extends Form_Field {

	public function convertValueToDbFormat($value) {
		if (preg_match("#(\d+)\.(\d+)\.(\d+) (\d+):(\d+):(\d+)#si", $value, $args)) {
			return mktime($args[4], $args[5], $args[6], $args[2], $args[1], $args[3]);
		} elseif (is_numeric($value)) {
			return $value;
		} else {
			return strtotime($value);
		}
	}

	public function convertDbFormatToValue($value) {
		return date("d.m.Y H:i:s", $value);
	}

	public function render($type = "form") {
		if (!empty($this->customDecorator) and is_callable($this->customDecorator)) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			if ($type == "form") {
				return "<label>" . $this->displayName . "</label>\n<input type='text' name='" . htmlspecialchars($this->fieldName, null, "UTF-8") . "' value='" . htmlspecialchars($this->currentValue, null, "UTF-8") . "'>\n<span class='help-block'>" . $this->inlineHelp . "</span>";
			} elseif ($type == "list") {
				return htmlspecialchars($this->currentValue, null, "UTF-8");
			}
		}
	}
}