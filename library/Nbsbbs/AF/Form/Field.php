<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 02.03.13
 * Time: 21:17
 */
namespace Nbsbbs\AF;

abstract class Form_Field {
	/**
	 * @var string Имя поля для формы
	 */
	public $fieldName;
	/**
	 * @var string Отображаемое названия в форме
	 */
	public $displayName;
	/**
	 * @var Form_Field_Constraint[]
	 */
	public $constraints = array();
	/**
	 * @var string Подстрочный хелп для поля
	 */
	public $inlineHelp;
	/**
	 * @var bool true, если поле обязательное
	 */
	public $isMandatory;
	/**
	 * @var bool true, если поле не показывается при отображении списком
	 */
	public $isListHidden;
	/**
	 * @var bool true, если поле не показывается при добавлении записи
	 */
	public $isAddHidden;
	/**
	 * @var bool|callable Функция-декоратор (отрисовывает элемент формы)
	 */	
	public $customDecorator = false;
	public $defaultValue;
	public $currentValue;

	public function convertValueToDbFormat($value) {
		return $value;
	}

	public function convertDbFormatToValue($value) {
		return $value;
	}

	public function setDecorator($decorator) {
		$this->customDecorator = $decorator;
	}

	public function render($type = "form") {
		if ($this->customDecorator) {
			return call_user_func_array($this->customDecorator, array($this, $type));
		} else {
			throw new \Exception("No decorator is set", 500);
		}
	}
}