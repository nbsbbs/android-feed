<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:25
 */
namespace Nbsbbs\AF;

class Model_Donor implements IFace_Crud, IFace_Listable {
	/**
	 * @var Form_Field[] Список полей формы
	 */
	private $formFields = array();
	private $additionalFields = array();

	public function __construct() {
		$id = new Form_Field_Id();
		$id->isListHidden = true;
		$this->formFields['id'] = $id;

		$donor_title = new Form_Field_String();
		$donor_title->fieldName = "donor_title";
		$donor_title->displayName = "Название";
		$donor_title->constraints[] = new Form_Field_Constraint_NotEmpty();
		$this->formFields['donor_title'] = $donor_title;

		$donor_manager = new Form_Field_Select();
		$donor_manager->fieldName = "donor_manager";
		$donor_manager->displayName = "Класс-обработчик";
		$donor_manager->constraints[] = new Form_Field_Constraint_ValidId("af_grabber_managers");
		$donor_manager->setSourceTable("af_grabber_managers");
		$donor_manager->setFieldsIndex("id", "display_name");
		$this->formFields['donor_manager'] = $donor_manager;
		
		$grab_freq_h = new Form_Field_String();
		$grab_freq_h->fieldName = "grab_freq_h";
		$grab_freq_h->displayName = "Частота граба (в часах)";
		$grab_freq_h->constraints[] = new Form_Field_Constraint_NotEmpty();
		$this->formFields['grab_freq_h'] = $grab_freq_h;
		
		$grab_error_m = new Form_Field_String();
		$grab_error_m->fieldName = "grab_error_m";
		$grab_error_m->displayName = "Точность в минутах";
		$this->formFields['grab_error_m'] = $grab_error_m;

		$next_grab = new Form_Field_Timestamp();
		$next_grab->fieldName = "next_grab";
		$next_grab->displayName = "Следующий граб";
		$next_grab->isAddHidden = true;
		$this->formFields['next_grab'] = $next_grab;

		$has_categories = new Form_Field_Checkbox();
		$has_categories->fieldName = "has_categories";
		$has_categories->displayName = "Есть категории?";
		$this->formFields['has_categories'] = $has_categories;

		$is_active = new Form_Field_Checkbox();
		$is_active->fieldName = "is_active";
		$is_active->displayName = "Активен?";
		$this->formFields['is_active'] = $is_active;

		$do_regrab = new Form_Field_Checkbox();
		$do_regrab->fieldName = "do_regrab";
		$do_regrab->displayName = "Реграб?";
		$this->formFields['do_regrab'] = $do_regrab;

	}
	
	public function scheduleNextGrab($donor_id) {
		if ($item = $this->getItemById($donor_id)) {
			$item['next_grab'] = time()+$item['grab_freq_h']*3600+intval(($item['grab_error_m']*60)*(mt_rand(-10000, 10000)/100000));
			$this->updateItem($donor_id, $item);
		} else throw new \Exception("Donor {$donor_id} not found");
	}

	public function addAdditionalField(Form_Field $field) {
		$this->additionalFields[] = $field;
	}

	/**
	 * Выдает список доноров, которых пришла пора грабить
	 * 
	 * @return array
	 */
	public function getDonorsToGrab() {
		$stmt = Registry::getInstance()->db->prepare("SELECT * from af_donors where next_grab<? and is_active=?");
		$stmt->execute(array(time(), 1));
		$collection = array();
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			$collection[] = $row;
		}
		return $collection;
	}

	/**
	 * Выдает список доноров для реграба
	 * 
	 * @return array
	 */
	public function getDonorsToRegrab() {
		$stmt = Registry::getInstance()->db->prepare("SELECT * from af_donors where do_regrab=? and is_active=?");
		$stmt->execute(array(1, 1));
		$collection = array();
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			$collection[] = $row;
		}
		return $collection;
	}
	
	/**
	 * Установить флаг реграба
	 * 
	 * @param int $donor_id ID донора
	 * @param bool $value [optional] false чтобы отменить реграб, true чтобы задать
	 */
	public function setRegrab($donor_id, $value = true) {
		$item = $this->getItemById($donor_id);
		$item['do_regrab'] = ($value) ? 1 : 0;
		$this->updateItem($donor_id, $item);
		
	}

	/**
	 * Возвращает список полей для данного типа
	 *
	 * @return Form_Field[]
	 */
	public function getFields() {
		return $this->formFields;
	}

	/**
	 * Добавляет запись в таблицу
	 *
	 * @param $item
	 * @return int id новой записи
	 */
	public function addItem($item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			$paramsArray[] = $field->fieldName . "=?";
			$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
		}
		$stmt = Registry::getInstance()->db->prepare("INSERT INTO af_donors SET " . implode(", ", $paramsArray));
		$stmt->execute($valsArray);
		return Registry::getInstance()->db->lastInsertId();
	}

	/**
	 * Удаляет запись из таблицы
	 *
	 * @param int $itemId
	 * @return bool
	 */
	public function deleteItem($itemId) {
		$stmt = Registry::getInstance()->db->prepare("DELETE FROM af_donors where id=?");
		$stmt->execute(array($itemId));
		return ($stmt->rowCount() > 0);
	}

	/**
	 * Обновляет запись в таблице
	 *
	 * @param int $itemId
	 * @param $item
	 * @return bool
	 */
	public function updateItem($itemId, $item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			$paramsArray[] = $field->fieldName . "=?";
			$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
		}
		$valsArray[] = $itemId;
		$stmt = Registry::getInstance()->db->prepare("UPDATE af_donors SET " . implode(", ", $paramsArray) . " WHERE id=?");
		$stmt->execute($valsArray);
		return ($stmt->rowCount() > 0);
	}

	/**
	 * @return int Общее число элементов
	 */
	public function getTotalElements() {
		$stmt = Registry::getInstance()->db->prepare("SELECT count(*) from af_donors");
		$stmt->execute();
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row['count(*)'];
		} else return 0;
	}

	/**
	 * Возвращает несколько элементов
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param string $orderBy название поля для сортировки
	 * @return array();
	 */
	public function getElements($limit, $offset, $orderBy = "") {
		if (!empty($orderBy) and preg_match("#^[\w\d_]$#si", $orderBy)) {
			$orderBy = "ORDER BY " . $orderBy;
		} else {
			$orderBy = "";
		}
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors " . $orderBy . " LIMIT ? OFFSET ?");
		$stmt->execute(array($limit, $offset));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Рисует интерфейс для добавления элемента
	 */
	public function drawAddForm() {
		// TODO: Implement drawAddForm() method.
	}

	/**
	 * Рисует интерфейс для редактирования элемента
	 */
	public function drawEditForm() {
		// TODO: Implement drawEditForm() method.
	}

	/**
	 * Возвращает элемент по id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function getItemById($id) {
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors where id=?");
		$stmt->execute(array($id));
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row;
		} else return false;
	}
}