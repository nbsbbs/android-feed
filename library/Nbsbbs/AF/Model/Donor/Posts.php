<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 05.03.13
 * Time: 20:59
 */
namespace Nbsbbs\AF;

class Model_Donor_Posts implements IFace_Crud, IFace_Listable {
	protected $filter = "";

	public function __construct() {
		$id = new Form_Field_Id();
		$id->isListHidden = true;
		$this->formFields['id'] = $id;

		$donor_id = new Form_Field_Select();
		$donor_id->fieldName = "donor_id";
		$donor_id->displayName = "Донор";
		$donor_id->constraints[] = new Form_Field_Constraint_ValidId("af_donors");
		$donor_id->setSourceTable("af_donors");
		$donor_id->setFieldsIndex("id", "donor_title");
		$this->formFields['donor_id'] = $donor_id;

		$post_guid = new Form_Field_String();
		$post_guid->fieldName = "post_guid";
		$post_guid->displayName = "GUID";
		$post_guid->isListHidden = true;
		$post_guid->constraints[] = new Form_Field_Constraint_NotEmpty();
		$this->formFields['post_guid'] = $post_guid;

		$donor_categories = new Form_Field_MultiSelect();
		$donor_categories->setSourceTable("af_donors_categories");
		$donor_categories->setFieldsIndex("id", "category_name");
		$donor_categories->fieldName = "donor_categories";
		$donor_categories->displayName = "Категории";
		$donor_categories->bindDataColumnName = "category_id";
		$donor_categories->bindItemColumnName = "post_id";
		$donor_categories->bindTableName = "af_donors_posts_categories";
		$this->formFields['donor_categories'] = $donor_categories;

		$post_title = new Form_Field_String();
		$post_title->fieldName = "post_title";
		$post_title->displayName = "Заголовок";
		$this->formFields['post_title'] = $post_title;

		$post_text = new Form_Field_Text();
		$post_text->fieldName = "post_text";
		$post_text->displayName = "Текст";
		$post_text->isListHidden = true;
		$this->formFields['post_text'] = $post_text;

		$post_pubtime = new Form_Field_Timestamp();
		$post_pubtime->fieldName = "post_pubtime";
		$post_pubtime->displayName = "Дата публикации";
		$this->formFields['post_pubtime'] = $post_pubtime;

		$post_grabtime = new Form_Field_Timestamp();
		$post_grabtime->fieldName = "post_grabtime";
		$post_grabtime->displayName = "Дата публикации";
		$this->formFields['post_grabtime'] = $post_grabtime;

		$additional_fields = new Form_Field_Text();
		$additional_fields->fieldName = "additional_fields";
		$additional_fields->displayName = "Доп. поля";
		$additional_fields->isListHidden = true;
		$additional_fields->isAddHidden = true;
		$this->formFields['additional_fields'] = $additional_fields;
	}

	/**
	 * Устанавливает фильтр по донору
	 * 
	 * @param int $donor_id ID донора
	 */
	public function setFilterByDonorId($donor_id) {
		$this->setFilter("AND donor_id=".intval($donor_id));
	}

	/**
	 * Сбрасывает фильтр на результаты запроса
	 */
	public function clearFilter() {
		$this->filter = "";
	}
	
	/**
	 * Устанавливает фильтр на результаты
	 * 
	 * @param string $filterStr SQL-строка для фильтрации запросов
	 */
	public function setFilter($filterStr) {
		$this->filter = $filterStr;
	}

	/**
	 * Возвращает список полей для данного типа
	 *
	 * @return Form_Field[]
	 */
	public function getFields() {
		return $this->formFields;
	}

	/**
	 * Добавляет запись в таблицу
	 *
	 * @param $item
	 * @return int id новой записи
	 */
	public function addItem($item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			if (!($field instanceof IFace_SeparateData)) {
				$paramsArray[] = $field->fieldName . "=?";
				$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
			} 
		}
		$stmt = Registry::getInstance()->db->prepare("INSERT INTO af_donors_posts SET " . implode(", ", $paramsArray));
		$stmt->execute($valsArray);
		$lastInsertId = Registry::getInstance()->db->lastInsertId();
		foreach ($this->getFields() as $field) {
			if (($field instanceof IFace_SeparateData) and ($field instanceof Form_Field)) {
				$field->saveSData($lastInsertId, $item[$field->fieldName]);
			}
		}
		return $lastInsertId;
	}

	/**
	 * Удаляет запись из таблицы
	 *
	 * @param int $itemId
	 * @return bool
	 */
	public function deleteItem($itemId) {
		$stmt = Registry::getInstance()->db->prepare("DELETE FROM af_donors_posts where id=?");
		$stmt->execute(array($itemId));
		$bool = $stmt->rowCount() > 0;
		foreach ($this->getFields() as $field) {
			if ($field instanceof IFace_SeparateData) {
				$field->clearSData($itemId); 
			}
		}
		return $bool;
	}

	/**
	 * Обновляет запись в таблице
	 *
	 * @param int $itemId
	 * @param $item
	 * @return bool
	 */
	public function updateItem($itemId, $item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			if (!($field instanceof IFace_SeparateData)) {
				$paramsArray[] = $field->fieldName . "=?";
				$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
			}
		}
		$valsArray[] = $itemId;
		$stmt = Registry::getInstance()->db->prepare("UPDATE af_donors_posts SET " . implode(", ", $paramsArray) . " WHERE id=?");
		$stmt->execute($valsArray);
		$bool = $stmt->rowCount() > 0;
		foreach ($this->getFields() as $field) {
			if (($field instanceof IFace_SeparateData) and ($field instanceof Form_Field)) {
				$field->clearSData($itemId);
				$field->saveSData($itemId, $item[$field->fieldName]);
			}
		}
		return $bool;
	}

	/**
	 * Рисует интерфейс для добавления элемента
	 */
	public function drawAddForm() {
		// TODO: Implement drawAddForm() method.
	}

	/**
	 * Возвращает элемент по id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function getItemById($id) {
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors_posts where id=?");
		$stmt->execute(array($id));
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row;
		} else return false;
	}

	/**
	 * Рисует интерфейс для редактирования элемента
	 */
	public function drawEditForm() {
		// TODO: Implement drawEditForm() method.
	}

	/**
	 * @return int Общее число элементов
	 */
	public function getTotalElements() {
		$stmt = Registry::getInstance()->db->prepare("SELECT count(*) from af_donors_posts WHERE 1 {$this->filter}");
		$stmt->execute();
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row['count(*)'];
		} else return 0;
	}

	/**
	 * Возвращает несколько элементов
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param string $orderBy название поля для сортировки
	 * @return array();
	 */
	public function getElements($limit, $offset, $orderBy = "") {
		if (!empty($orderBy) and preg_match("#^[\w\d_]$#si", $orderBy)) {
			$orderBy = "ORDER BY " . $orderBy;
		} else {
			$orderBy = "";
		}
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors_posts WHERE 1 {$this->filter} " . $orderBy . " LIMIT ? OFFSET ?");
		$stmt->execute(array($limit, $offset));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Вставляет в таблицу af_donors_posts найденные элементы
	 *
	 * @param int $donor_id
	 * @param Bean_Grabber_Entry[] $entries
	 * @throws \Exception
	 * @return int Число вставленных записей
	 */
	public function insertEntries($donor_id, $entries) {
		$cnt = 0;
		$modelDonorCategories = new Model_Donor_Categories();
		foreach ($entries as $entry) {
			// TODO: избавиться от $entry->additionalParams
			$categoryIds = array();
			foreach ($entry->categories as $tmp) {
				$categoryIds[] = $tmp->id;
			}
			$item = array("donor_id" => $donor_id, "post_guid" => $entry->guid, "post_title" => $entry->title, "donor_categories" => $categoryIds, "post_text" => $entry->text, "post_pubtime" => $entry->pubTime, "post_grabtime" => $entry->grabTime, "additional_fields" => $entry->paramsToString($entry->additionalParams));
			try {
				$storyId = $this->addItem($item);
				foreach($entry->categories as $tmp) {
					$modelDonorCategories->bindCategoryToStory($tmp->id, $storyId);
				}
				$cnt++;
			} catch (\Exception $e) {
				if ($e->getCode() == 23000) {
					// ignore duplicate key
				} else throw $e;
			}
		}
		return $cnt;
	}

	public function getNewEntries($donor_id) {
		$modelDonor = new Model_Donor();
		$fields = $modelDonor->getFields();
		if ($donorInfo = $modelDonor->getItemById($donor_id)) {
			// все ок
			foreach ($fields as $fieldName => $field) {
				$field->currentValue = $field->convertDbFormatToValue($donorInfo[$fieldName]);
				$fields[$fieldName] = $field;
			}
		} else {
			throw new \Exception("Донор номер {$donor_id} не найден");
		}

		if ($fields['donor_manager'] instanceof Form_Field_Select) {
			$factory = new Grabber_Factory();
			$grabber = $factory->getGrabber($fields['donor_manager']->getDisplayValueByIndex($fields['donor_manager']->currentValue));
			$grabber->setDonorId($fields['id']->currentValue);
		} else {
			throw new \Exception("Invalid field['donor_manager'] type ");
		}
		return $this->insertEntries($donor_id, $grabber->getNewEntries());
	}

	public function getAllEntries($donor_id) {
		$cnt = 0;
		$modelDonor = new Model_Donor();
		$fields = $modelDonor->getFields();
		if ($donorInfo = $modelDonor->getItemById($donor_id)) {
			// все ок
			foreach ($fields as $fieldName => $field) {
				$field->currentValue = $field->convertDbFormatToValue($donorInfo[$fieldName]);
				$fields[$fieldName] = $field;
			}
		} else {
			throw new \Exception("Донор номер {$donor_id} не найден");
		}

		if ($fields['donor_manager'] instanceof Form_Field_Select) {
			$factory = new Grabber_Factory();
			$grabber = $factory->getGrabber($fields['donor_manager']->getDisplayValueByIndex($fields['donor_manager']->currentValue));
			$grabber->setDonorId($fields['id']->currentValue);
		} else {
			throw new \Exception("Invalid field['donor_manager'] type ");
		}

		try {
			$entries = $grabber->getAllEntries();
			$cnt = $this->insertEntries($donor_id, $entries);
		} catch (\Exception $e) {
			echo "Exception:\n";
			echo $e->getCode()."\n";
			echo $e->getMessage()."\n";
			$cnt = 0;
		}
		
		return $cnt;
	}

}