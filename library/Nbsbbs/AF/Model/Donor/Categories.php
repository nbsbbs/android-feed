<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 05.03.13
 * Time: 20:43
 */
namespace Nbsbbs\AF;

class Model_Donor_Categories implements IFace_Crud, IFace_Listable {

	public function __construct() {
		$id = new Form_Field_Id();
		$id->isListHidden = true;
		$this->formFields['id'] = $id;

		$donor_id = new Form_Field_Select();
		$donor_id->fieldName = "donor_id";
		$donor_id->displayName = "Донор";
		$donor_id->constraints[] = new Form_Field_Constraint_ValidId("af_donors");
		$donor_id->setSourceTable("af_donors");
		$donor_id->setFieldsIndex("id", "donor_title");
		$this->formFields['donor_id'] = $donor_id;
		
		$parent_id = new Form_Field_Select();
		$parent_id->fieldName = "parent_id";
		$parent_id->displayName = "Категория-родитель";
		$parent_id->setSourceTable("ad_donors_categories");
		$parent_id->setFieldsIndex("id", "category_name");
		$this->formFields['parent_id'] = $parent_id;

		$category = new Form_Field_String();
		$category->fieldName = "category_name";
		$category->displayName = "Категория";
		$category->constraints[] = new Form_Field_Constraint_NotEmpty();
		$this->formFields['category_name'] = $category;
	}

	/**
	 * Возвращает список полей для данного типа
	 *
	 * @return Form_Field[]
	 */
	public function getFields() {
		return $this->formFields;
	}

	/**
	 * @param int $donorId
	 * @param string $name
	 * @param int $parentId [optional] ID родительской категории
	 * @return int id категории из af_donors_categories
	 */
	public function getCategoryIdByName($donorId, $name, $parentId = 0) {
		if (!empty($parentId)) {
			$stmt = Registry::getInstance()->db->prepare("SELECT id FROM af_donors_categories WHERE donor_id=? and category_name=? and parent_id=?");
			$stmt->execute(array($donorId, $name, $parentId));
		} else {
			$stmt = Registry::getInstance()->db->prepare("SELECT id FROM af_donors_categories WHERE donor_id=? and category_name=?");
			$stmt->execute(array($donorId, $name));
		}
		if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
			return $row[0];
		} else {
			return $this->addItem(array("donor_id" => $donorId, "category_name" => $name, "parent_id" => $parentId));
		}
	}

	/**
	 * @param int $category_id
	 * @param int $story_id
	 * @return bool
	 */
	public function bindCategoryToStory($category_id, $story_id) {
		$stmt = Registry::getInstance()->db->prepare("INSERT INTO af_donors_posts_categories (post_id, category_id) values (?, ?)");
		$stmt->execute(array($story_id, $category_id));
		return true;
	}
	
	/**
	 * Добавляет запись в таблицу
	 *
	 * @param $item
	 * @return int id новой записи
	 */
	public function addItem($item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			$paramsArray[] = $field->fieldName . "=?";
			$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
		}
		$stmt = Registry::getInstance()->db->prepare("INSERT INTO af_donors_categories SET " . implode(", ", $paramsArray));
		$stmt->execute($valsArray);
		return Registry::getInstance()->db->lastInsertId();
	}

	/**
	 * Удаляет запись из таблицы
	 *
	 * @param int $itemId
	 * @return bool
	 */
	public function deleteItem($itemId) {
		$stmt = Registry::getInstance()->db->prepare("DELETE FROM af_donors_categories where id=?");
		$stmt->execute(array($itemId));
		return ($stmt->rowCount() > 0);
	}

	/**
	 * Обновляет запись в таблице
	 *
	 * @param int $itemId
	 * @param $item
	 * @return bool
	 */
	public function updateItem($itemId, $item) {
		$paramsArray = array();
		$valsArray = array();
		foreach ($this->getFields() as $field) {
			if ($field instanceof Form_Field_Id) continue;
			$paramsArray[] = $field->fieldName . "=?";
			$valsArray[] = $field->convertValueToDbFormat($item[$field->fieldName]);
		}
		$valsArray[] = $itemId;
		$stmt = Registry::getInstance()->db->prepare("UPDATE af_donors_categories SET " . implode(", ", $paramsArray) . " WHERE id=?");
		$stmt->execute($valsArray);
		return Registry::getInstance()->db->lastInsertId();
	}

	/**
	 * Рисует интерфейс для добавления элемента
	 */
	public function drawAddForm() {
		// TODO: Implement drawAddForm() method.
	}

	/**
	 * Возвращает элемент по id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function getItemById($id) {
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors_categories where id=?");
		$stmt->execute(array($id));
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row;
		} else return false;
	}

	/**
	 * Рисует интерфейс для редактирования элемента
	 */
	public function drawEditForm() {
		// TODO: Implement drawEditForm() method.
	}

	/**
	 * @return int Общее число элементов
	 */
	public function getTotalElements() {
		$stmt = Registry::getInstance()->db->prepare("SELECT count(*) from af_donors_categories");
		$stmt->execute();
		if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			return $row['count(*)'];
		} else return 0;
	}

	/**
	 * Возвращает несколько элементов
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param string $orderBy название поля для сортировки
	 * @return array();
	 */
	public function getElements($limit, $offset, $orderBy = "") {
		if (!empty($orderBy) and preg_match("#^[\w\d_]$#si", $orderBy)) {
			$orderBy = "ORDER BY " . $orderBy;
		} else {
			$orderBy = "";
		}
		$stmt = Registry::getInstance()->db->prepare("SELECT * FROM af_donors_categories " . $orderBy . " LIMIT ? OFFSET ?");
		$stmt->execute(array($limit, $offset));
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}