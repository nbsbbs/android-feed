<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:31
 */
namespace Nbsbbs\AF;

class View {

	public function __construct() {
		$this->registry = Registry::getInstance();
	}

	// получить отренедеренный шаблон с параметрами $params
	function fetch($template, $params = array()) {
		$_templateText = $this->loadTemplate($template);
		$_templateText = $this->parseBlocks($_templateText);
		$_templateText = $this->stripBlocks($_templateText);
		extract($params);
		ob_start();
		eval("?>" . $_templateText);
		return ob_get_clean();
	}

	function render($template, $params = array()) {
		echo $this->fetch($template, $params);
	}

	function loadTemplate($template) {
		return file_get_contents($this->registry->config->viewsBasedir . "/" . $this->registry->language . "/" . $template . '.php');
	}

	function stripBlocks($text) {
		$text = preg_replace("#{extends([^}]+)}#sui", "", $text);
		$text = preg_replace("#{block([^}]+)}(.+?){/block}#sui", "$2", $text);
		return $text;
	}

	function parseBlocks($text) {
		if (preg_match("#{extends\s+template=\"([^\"]+)\"\s*}#siu", $text, $ext)) {
			//load local blocks
			$blocks = array();
			preg_match_all("#{block\s+name=\\\"([^\"]+)\\\"\s*}(.+?){/block}#siu", $text, $args, PREG_SET_ORDER);
			for ($i = 0; $i < sizeof($args); $i++) {
				$blocks[$args[$i][1]] = $args[$i][2];
			}

			// load initial template
			$upperTemplate = $this->loadTemplate($ext[1]);

			// replace blocks
			foreach ($blocks as $blockName => $blockText) {
				$upperTemplate = preg_replace("#({block\s+name=\\\"" . $blockName . "\\\"})(.+?)({/block})#siu", "$1" . $blockText . "$3", $upperTemplate);
			}

			return $this->parseBlocks($upperTemplate);
		}
		return $text;
	}

}