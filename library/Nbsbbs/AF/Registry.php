<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:07
 */
namespace Nbsbbs\AF;

class Registry {
	static $instance;
	/**
	 * @var \PDO $db;
	 */
	public $db;
	public $language = "ru";
	/**
	 * @var \StdClass
	 */
	public $config;

	public static function getInstance() {
		if (!self::$instance) {
			self::$instance = new Registry();
			self::$instance->config = new \StdClass;
		}
		return self::$instance;
	}
}