<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:43
 */
namespace Nbsbbs\AF;

class Controller {
	public $view, $registry;

	function __construct() {
		$this->registry = Registry::getInstance();
		$this->view = new View();
	}

	public function buildUrl() {
		$args = func_get_args();
		return "/" . $this->registry->config->baseUrl . implode("/", array_map("rawurlencode", $args));
	}

	public function redirect($url) {
		ob_clean();
		header("Location: " . $url);
		die();
	}

	// другие полезные методы вроде redirect($url);
}