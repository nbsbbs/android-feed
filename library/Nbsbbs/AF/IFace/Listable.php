<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:22
 */
namespace Nbsbbs\AF;

interface IFace_Listable {

	/**
	 * @return int Общее число элементов
	 */
	public function getTotalElements();

	/**
	 * Возвращает несколько элементов
	 *
	 * @param int $limit
	 * @param int $offset
	 * @param string $orderBy название поля для сортировки
	 * @return array();
	 */
	public function getElements($limit, $offset, $orderBy = "");
}