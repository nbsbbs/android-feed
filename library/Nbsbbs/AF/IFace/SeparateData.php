<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 15.03.13
 * Time: 20:04
 */
namespace Nbsbbs\AF;

interface IFace_SeparateData {

	/**
	 * @param int $itemId ID элемента
	 * @param mixed $data Сохраняемые данные
	 * @return boolean Успешно или нет
	 */
	public function saveSData($itemId, $data);

	/**
	 * @param int $itemId ID элемента
	 * @return mixed[] Данные
	 */
	public function loadSData($itemId);

	/**
	 * @param int $itemId ID элемента
	 * @return void
	 */
	public function clearSData($itemId);
}