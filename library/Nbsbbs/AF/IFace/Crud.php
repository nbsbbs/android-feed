<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 14:15
 */
namespace Nbsbbs\AF;

interface IFace_Crud {
	/**
	 * Возвращает список полей для данного типа
	 *
	 * @return Form_Field[]
	 */
	public function getFields();

	/**
	 * Добавляет запись в таблицу
	 *
	 * @param $item
	 * @return int id новой записи
	 */
	public function addItem($item);

	/**
	 * Удаляет запись из таблицы
	 *
	 * @param int $itemId
	 * @return bool
	 */
	public function deleteItem($itemId);

	/**
	 * Обновляет запись в таблице
	 *
	 * @param int $itemId
	 * @param $item
	 * @return bool
	 */
	public function updateItem($itemId, $item);

	/**
	 * Рисует интерфейс для добавления элемента
	 */
	public function drawAddForm();

	/**
	 * Возвращает элемент по id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function getItemById($id);

	/**
	 * Рисует интерфейс для редактирования элемента
	 */
	public function drawEditForm();

}