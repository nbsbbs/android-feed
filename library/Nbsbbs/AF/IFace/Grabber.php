<?php
/**
 * Created by JetBrains PhpStorm.
 * Date: 02.03.13
 * Time: 21:00
 */
namespace Nbsbbs\AF;

interface IFace_Grabber {
	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries();

	/**
	 * Возвращает массив всех доступных записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries();

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields();
}
