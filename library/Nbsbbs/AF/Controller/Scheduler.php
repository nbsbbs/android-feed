<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 24.03.13
 * Time: 16:59
 */
namespace Nbsbbs\AF;

class Controller_Scheduler {
	
	public function work() {
		if ($this->getLock()) {
			$this->workRegrab();
			$this->workGrabScheduled();
		} else {
			echo "already running\n";
		}
	}
	
	public function workRegrab() {
		$stmt = Registry::getInstance()->db->prepare("SELECT id, donor_title, donor_manager from af_donors where do_regrab=?");
		$stmt->execute(array(1));
		$model = new Model_Donor_Posts();
		$donors = new Model_Donor();
		while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
			echo "Regrab donor ".$row['id']." (".$row['donor_title'].")\n";
			$model->getAllEntries($row['id']);
			$m = new Model_Donor();
			$m->setRegrab($row['id'], false);
			$donors->scheduleNextGrab($row['id']);
		}
	}

	public function workGrabScheduled() {
		$model = new Model_Donor_Posts();
		$donors = new Model_Donor();
		foreach ($donors->getDonorsToGrab() as $row) {
			echo "Grab donor ".$row['id']." (".$row['donor_title'].")\n";
			$model->getNewEntries($row['id']);
			$donors->scheduleNextGrab($row['id']);
		}
	}

	public function getLock() {
		if (!is_dir(dirname(PATHS_FILECACHE.DIRECTORY_SEPARATOR.__CLASS__.".lock"))) {
			mkdir(dirname(PATHS_FILECACHE.DIRECTORY_SEPARATOR.__CLASS__.".lock"), 0777, true);
		}
		return flock($this->lock = fopen(PATHS_FILECACHE.DIRECTORY_SEPARATOR.__CLASS__.".lock", 'w'), LOCK_EX | LOCK_NB);
	}
}
