<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:42
 */
namespace Nbsbbs\AF;

class Controller_Index extends Controller {

	public function actionIndex() {
		$this->view->render("index/subinner", array("controller" => __CLASS__));
	}
}