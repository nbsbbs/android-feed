<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 05.03.13
 * Time: 21:08
 */
namespace Nbsbbs\AF;

class Controller_Posts extends Controller {
	public function actionList($page = 1) {
		$model = new Model_Donor_Posts();
	
		$donor_id = abs(intval($_GET['donor_id']));
		if (!empty($donor_id)) {
			$model->setFilterByDonorId($donor_id);
		}
		
		$totalItems = $model->getTotalElements();
		$totalPages = ceil($totalItems / 50);
		$page = intval($page);
		if ($page < 1) $page = 1;
		$offset = ($page - 1) * 50;
		$items = $model->getElements(50, $offset, $_GET['order']);
		$fields = $model->getFields();

		$this->view->render("posts/list", array("controller" => __CLASS__, "fields" => $fields, "page" => $page, "totalPages" => $totalPages, "totalItems" => $totalItems, "items" => $items));
	}

	public function actionAdd() {
		if (empty($_POST)) {
			$model = new Model_Donor_Posts();
			$fields = $model->getFields();
			$this->view->render("posts/add", array("controller" => __CLASS__, "fields" => $fields));
		} else {
			$model = new Model_Donor_Posts();
			$fields = $model->getFields();
			foreach ($fields as $index => $field) {
				if ($field instanceof IFace_SeparateData) {
					$field->currentValue = $field->loadSData($_POST['id']);
				}
			}
			if (empty($_POST['id'])) {
				// check constraints
				$errors = $this->checkConstraints($fields, $_POST, "add");
				if (empty($errors)) {
					if ($id = $model->addItem($_POST)) {
						$this->redirect($this->buildUrl("posts", "edit", $id));
					} else {
						$this->view->render("error", array("controller" => __CLASS__, "errors" => array("Не удалось добавить донора ")));
					}
				} else {
					foreach ($fields as $fieldName => $field) {
						$field->currentValue = $field->convertDbFormatToValue($_POST[$fieldName]);
						$fields[$fieldName] = $field;
					}
					$this->view->render("posts/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => $errors));
				}
			} else {
				// check constraints
				foreach ($fields as $fieldName => $field) {
					$field->currentValue = $field->convertDbFormatToValue($_POST[$fieldName]);
					$fields[$fieldName] = $field;
				}
				if ($fields['donor_categories[]'] instanceof Form_Field_MultiSelect) $fields['donor_categories[]']->setFilter("donor_id=" . intval($_POST['donor_id']));
				$errors = $this->checkConstraints($fields, $_POST);
				if (empty($errors)) {
					if ($model->updateItem($_POST['id'], $_POST)) {
						$this->redirect($this->buildUrl("posts", "edit", $_POST['id']));
					} else {
						$this->view->render("posts/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => array("Изменений в таблице не сделано")));
					}
				} else {
					$this->view->render("posts/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => $errors));
				}
			}
		}
	}

	/**
	 * Проверяет массив с данными по констрейнтам
	 *
	 * @param Form_Field[] $fields
	 * @param array $data
	 * @param string $type (optional)
	 * @return array Ошибки
	 */
	public function checkConstraints($fields, $data, $type = "edit") {
		$errors = array();
		foreach ($fields as $field) {
			if (!empty($field->constraints)) {
				foreach ($field->constraints as $constraint) {
					if ($type == "add") {
						if ($field->isAddHidden) continue;
					}
					if (!$constraint->check($data[$field->fieldName])) {
						foreach ($constraint->getMessages() as $error) {
							$errors[] = $field->displayName . ": " . $error;
						}
					}
				}
			}
		}
		return $errors;
	}

	public function actionEdit($id) {
		$model = new Model_Donor_Posts();
		$item = $model->getItemById($id);
		$fields = $model->getFields();
		foreach ($fields as $fieldName => $field) {
			$field->currentValue = $field->convertDbFormatToValue($item[$fieldName]);
			$fields[$fieldName] = $field;
			if ($field instanceof IFace_SeparateData) {
				$field->currentValue = $field->loadSData($id);
			}
		}
		$additionalFields = array();
		$this->view->render("posts/add", array("controller" => __CLASS__, "item" => $item, "additionalFields" => $additionalFields, "fields" => $fields));
	}

	public function actionDelete($id) {
		$model = new Model_Donor_Posts();
		if ($model->deleteItem($id)) {
			$this->redirect($this->buildUrl("posts", "list"));
		} else {
			$this->view->render("error", array("controller" => __CLASS__, "errors" => array("Не удалось удалить пост донора номер " . intval($id))));
		}
	}
}