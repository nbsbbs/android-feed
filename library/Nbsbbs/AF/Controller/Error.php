<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 19:36
 */
namespace Nbsbbs\AF;

class Controller_Error extends Controller {

	public function actionError($errors = array()) {
		$this->view->render("error", array("controller" => __CLASS__, "errors" => $errors));
	}
}