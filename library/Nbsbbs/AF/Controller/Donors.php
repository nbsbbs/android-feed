<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 19:07
 */
namespace Nbsbbs\AF;

class Controller_Donors extends Controller {

	public function actionList($page = 1) {
		$model = new Model_Donor();
		$totalItems = $model->getTotalElements();
		$totalPages = ceil($totalItems / 50);
		$page = intval($page);
		if ($page < 1) $page = 1;
		$offset = ($page - 1) * 50;
		$items = $model->getElements(50, $offset, $_GET['order']);
		$fields = $model->getFields();

		$this->view->render("donors/list", array("controller" => __CLASS__, "fields" => $fields, "page" => $page, "totalPages" => $totalPages, "totalItems" => $totalItems, "items" => $items));
	}
	
	public function actionRegrab($id) {
		$model = new Model_Donor();
		$model->setRegrab($id, true);
				
		// удалить все посты и категории
		$stmt = Registry::getInstance()->db->prepare("DELETE FROM af_donors_posts_categories where post_id in (select id from af_donors_posts where donor_id=?)");
		$stmt->execute(array($id));
		$stmt = Registry::getInstance()->db->prepare("DELETE FROM af_donors_posts where donor_id=?");
		$stmt->execute(array($id));

		$this->redirect($this->buildUrl("donors", "list", 1));
	}

	public function actionAdd() {
		if (empty($_POST)) {
			$model = new Model_Donor();
			$fields = $model->getFields();
			$this->view->render("donors/add", array("controller" => __CLASS__, "fields" => $fields));
		} else {
			$model = new Model_Donor();
			$fields = $model->getFields();
			if (empty($_POST['id'])) {
				// check constraints
				$errors = $this->checkConstraints($fields, $_POST, "add");
				if (empty($errors)) {
					if ($id = $model->addItem($_POST)) {
						$this->redirect($this->buildUrl("donors", "edit", $id));
					} else {
						$this->view->render("error", array("controller" => __CLASS__, "errors" => array("Не удалось добавить донора ")));
					}
				} else {
					foreach ($fields as $fieldName => $field) {
						$field->currentValue = $field->convertDbFormatToValue($_POST[$fieldName]);
						$fields[$fieldName] = $field;
					}
					$this->view->render("donors/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => $errors));
				}
			} else {
				// check constraints
				foreach ($fields as $fieldName => $field) {
					$field->currentValue = $field->convertDbFormatToValue($_POST[$fieldName]);
					$fields[$fieldName] = $field;
				}
				$errors = $this->checkConstraints($fields, $_POST);
				if (empty($errors)) {
					if ($model->updateItem($_POST['id'], $_POST)) {
						$this->redirect($this->buildUrl("donors", "edit", $_POST['id']));
					} else {
						$this->view->render("donors/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => array("Изменений в таблице не сделано")));
					}
				} else {
					$this->view->render("donors/add", array("controller" => __CLASS__, "fields" => $fields, "errors" => $errors));
				}
			}
		}
	}

	/**
	 * Проверяет массив с данными по констрейнтам
	 *
	 * @param Form_Field[] $fields
	 * @param array $data
	 * @return array Ошибки
	 */
	public function checkConstraints($fields, $data) {
		$errors = array();
		foreach ($fields as $field) {
			if (!empty($field->constraints)) {
				foreach ($field->constraints as $constraint) {
					if (!$constraint->check($data[$field->fieldName])) {
						foreach ($constraint->getMessages() as $error) {
							$errors[] = $field->displayName . ": " . $error;
						}
					}
				}
			}
		}
		return $errors;
	}

	public function actionEdit($id) {
		$model = new Model_Donor();
		$item = $model->getItemById($id);
		$fields = $model->getFields();
		foreach ($fields as $fieldName => $field) {
			$field->currentValue = $field->convertDbFormatToValue($item[$fieldName]);
			$fields[$fieldName] = $field;
		}
		$additionalFields = array();
		$this->view->render("donors/add", array("controller" => __CLASS__, "item" => $item, "additionalFields" => $additionalFields, "fields" => $fields));
	}

	public function actionDelete($id) {
		$model = new Model_Donor();
		if ($model->deleteItem($id)) {
			$this->redirect($this->buildUrl("donors", "list"));
		} else {
			$this->view->render("error", array("controller" => __CLASS__, "errors" => array("Не удалось удалить донора номер " . intval($id))));
		}
	}
	
	public function actionEntries($donor_id) {
		$model = new Model_Donor_Posts();
		$model->getTotalElements();
	}
}