<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 14:29
 */
namespace Nbsbbs\AF;

class Grabber_Donor_Bashim extends Grabber_Donor {

	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries() {
		$maxPage = $this->getMaxPages();
		$collection = $this->parsePage($this->getUrlByPageNum($maxPage));
		if ($maxPage > 2) $collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum($maxPage - 1)));
		return $collection;
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
		$collection = array();
		foreach ($this->getPagesIterator() as $page) {
			try {
				$collection = array_merge($collection, $this->parsePage($page));
			} catch (\Exception $e) {
				echo "Exception: \n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}

	/**
	 * Возвращает итератор со страницами для парсинга
	 *
	 * @return \Iterator
	 */
	public function getPagesIterator() {
		$collection = array();
		$maxPages = $this->getMaxPages(); // нужно, чтобы не делать запросы в цикле
		for ($i = $maxPages; $i >= 1; $i--) {
			$collection[] = $this->getUrlByPageNum($i);
		}
		return new \ArrayIterator($collection);
	}

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields() {
		return array();
	}

	/**
	 * @return int Число страниц у башорга
	 * @throws \Exception
	 */
	public function getMaxPages() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://bash.im/")) {
			if (preg_match("#<span class=\"current\"><input type=\"text\" name=\"page\" class=\"page\" pattern=\"[^\"]+\" numeric=\"integer\" min=\"1\" max=\"[^\"]+\" value=\"(\d+)\" /></span>#si", $text, $args)) {
				return intval($args[1]);
			} else {
				throw new \Exception("Не найден шаблон для текущей страницы bash.im");
			}
		} else {
			throw new \Exception("Cannot fetch page http://bash.im/");
		}
	}

	/**
	 * Возвращает список записей из строки
	 *
	 * @param string $text Текст страницы
	 * @return Bean_Grabber_Entry[]
	 */
	public function parseText($text) {
		$collection = array();
		foreach (explode("<div class=\"quote\">", $text) as $entry) {
			if (strpos($entry, "<div class=\"actions\">") !== false) {
				if (preg_match("#<div class=\"text\">(.+?)</div>#si", $entry, $args)) {
					$text = iconv("cp1251", "utf-8", $args[1]);
					$text = preg_replace("(<br.*?>)", "\n", $text);
					$text = htmlspecialchars_decode($text);
					if (preg_match("#<a href=\"/quote/(\d+)/rulez#si", $entry, $args)) {
						$guid = $args[1];
						$title = "Цитата #" . $guid;
						if (preg_match("#<span class=\"date\">(.+?)</span>#si", $entry, $args)) {
							$date = $args[1];
						}
						$bean = new Bean_Grabber_Entry();
						$bean->grabTime = time();
						$bean->guid = $guid;
						$bean->text = $text;
						$bean->title = $title;
						$bean->pubTime = strtotime($date);
						$bean->additionalParams = array("random" => mt_rand(1,10000));
						$collection[] = $bean;
					}
				} else {
					echo "Strange piece\n";
					echo $entry . "\n\n\n\n";
				}
			}
		}
		return $collection;
	}

	/**
	 * Возвращает URL страницы по номеру
	 *
	 * @param int $pageNum
	 * @return string
	 */
	public function getUrlByPageNum($pageNum) {
		return "http://bash.im/index/" . $pageNum;
	}

	/**
	 * Возвращает список записей на конкретной странице
	 *
	 * @param string $page Bean_Grabber_Entry страницы
	 * @return Bean_Grabber_Entry[]
	 * @throws \Exception
	 */
	public function parsePage($page) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}
}