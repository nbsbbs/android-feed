<?php

/**
 * @author tvolf
 * Date: 28.03.13
 * Time: 19:30
 */

namespace Nbsbbs\AF;

class Grabber_Donor_1001FactsInfo extends Grabber_Donor {

	private $monthYearList = array();
    private $currentPage = array();

	public function getAdditionalFields() {
        $fields = array();      
        $img = new Form_Field_Text();    
        $img->fieldName = "img";    
        $img->displayName = "URLы картинок";    
        $fields['img'] = $img;    
        return $fields;   
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
        
        $result = $this->parseArticle('http://1001facts.info/chemical/');
        print_r($result);
        die();
        
        if (empty($this->monthYearList)) {
            $this->monthYearList = $this->getMonthYearList();
        }
        
		$collection = array();
//        $pages = array_slice($this->monthYearList, 0, 4);
//        foreach ($pages as $page) {
		foreach ($this->getPagesIterator() as $page) {
			try {
                $this->currentPage = $page;
                $text = $this->parsePage($page['link']);
                $collection = array_merge($collection, $this->parseText($text));
			} catch (\Exception $e) {
				echo "Exception:" . $e->getMessage() . "\n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}
    
    
	public function getPagesIterator() {
        if (empty($this->monthYearList)) {
            $this->monthYearList = $this->getMonthYearList();
        }
		$result = array();
        foreach($this->monthYearList as $item) {
            $baseUrl = $item['link'];
            $maxNum = $this->getMaxPage($baseUrl);
            for ($i = 1; $i <= $maxNum;  ++$i) {
                $result[] = array('link' => $this->getUrlById($baseUrl, $i),
                                  'title' => $item['title'] 
                                  );
            }
        }
		return $result;
	}

	public function getNewEntries() {
        if (empty($this->monthYearList)) {
            $this->monthYearList = $this->getMonthYearList();
        }
    	$r = new AsyncSocketRequest();

        $pages = array();
        for($i = 0; $i <= 1; ++$i) {
            $baseUrl = $this->monthYearList[$i]['link'];
            $maxPage = $this->getMaxPage($baseUrl);    
            for ($j = 1; $j <= $maxPage; ++$j ) {
                $pages[] = array('link'  => $this->getUrlById($baseUrl, $j),
                                 'title' => $this->monthYearList[$i]['title']
                                 );
            }
        }
		$block = array();        
        foreach ($pages as $page) {
            $pageUrl = $page['link'];
            $pageTitle = $page['title'];
            $this->currentPage = array('link'  => $pageUrl, 
                                       'title' => $pageTitle
                                       );
            if ($text = $r->get($pageUrl)) {
                $block = array_merge($block, $this->parseText($text));
            } else {
                throw new \Exception("Cannot fetch page " . $pageUrl);
            }
        }
		return $block;
	}
    

    
    private function parsePage($url) {
        $r = new AsyncSocketRequest();
        if (!($text = $r->get($url))) {
            throw new \Exception('cannot fetch page ' . $url);
        }
        return $text;
    }

	/**
	 * Получение списка всех месяцев/годов сайта 1001FFacts.info
	 * @return array список все месяцев/годов сайта 1001FFacts.info
	 * @throws \Exception
	 */
	private function getMonthYearList() {
		$baseUrl = 'http://1001facts.info/';
		$monthYearList = array();
		$r = new AsyncSocketRequest();
		$text = $r->get($baseUrl);
		if ($text == FALSE) {
			throw new \Exception("Cannot fetch page " . $baseUrl);
		}
		$dom = str_get_html($text);
		if ($ul = $dom->find("ul.sidebarlist", 1)) {
			foreach ($ul->find('li') as $li) {
                if ($link = $li->find('a', 0)) {
                    $monthYearList[] = array(
                        'link'  => $link->href,
                        'title' => $link->plaintext
                    );
                } else {
                    throw new \Exception("Cannot get MonthYear from page " . $baseUrl);                    
                }
			}
		} else {
			throw new \Exception("Cannot get MonthYear from page " . $baseUrl);
		}
		return $monthYearList;
	}

	/**
	 * Функция возвращает максимальный номер страницы для заданной категории сайта 1001facts.info
	 * @param  string $baseLink     базовый url категории
	 * @return int  Максимальный номер страницы сайта заданной категории
	 * @throws \Exception
	 */
	private function getMaxPage($baseLink) {
		$r = new AsyncSocketRequest();
		$maxPage = -1;
        $args = array();
		if ($text = $r->get($baseLink)) {
			$dom = str_get_html($text);
			if ($lastLink = $dom->find(".wp-pagenavi a.last", 0)) {
                if (preg_match("#" . $baseLink . "page/(.*?)/#is", $lastLink->href, $args)) {
                    $maxPage = intval($args[1]);                    
                }
			} else {
                if ($lastLink = $dom->find(".wp-pagenavi a.page", -1)) {
                    $maxPage = intval($lastLink->plaintext);                    
                } else {
                    if ($dom->find('h1.maintitle a.free')) { // имеются хоть какие-то статьи на странице
                        $maxPage = 1;
                    }
                }
            }
        }     
        if ($maxPage == -1) {
            throw new \Exception("Cannot find max page number for " . $baseLink);
        }
		return $maxPage;
	}

	/**
	 * Функция возвращает url страницы по её категории и id
	 * @param string $baseUrl базовый url категории
	 * @param int $id    id страницы
	 * @return string    url страницы
	 */
	private function getUrlById($baseUrl, $id) {
		if ($id == 1) {
			$url = $baseUrl;
		} else {
			$url = $baseUrl . "page/" . $id . "/";
		}
		return $url;
	}

    
    public function parseText($text) {
		$result = array();
		$catsModel = new Model_Donor_Categories();        
        $page = $this->currentPage;
        $dom = str_get_html($text);
        foreach ($dom->find('h1.maintitle a.free') as $link) {
            $data = $this->parseArticle($link->href, $page);
            $bean = new Bean_Grabber_Entry();
            $bean->grabTime = time();
            $bean->text = $data['body'];
            $bean->guid = $data['guid'];
            $bean->title = $data['title'];
            $bean->categories = array();
            $bean->pubTime = $data['pubTime'];
            $bean->additionalParams =  array('images' => $data['images']);
            foreach ($data['categories'] as $categoryName) {
                $catBean = new Bean_Donor_Category();
                $catBean->name = trim($categoryName);
                $categoryId = $catsModel->getCategoryIdByName($this->donorId, $catBean->name);
                $categoryItem = $catsModel->getItemById($categoryId);
                $catBean->id = $categoryItem['id'];
                $catBean->parentId = $categoryItem['parent_id'];
                $bean->categories[] = $catBean;
            }
            $result[] = $bean;
        }
		return $result;
	}
    
    /**
     * Парсинг одной статьи сайта http://1001facts.info
     * @param string $url url на отдельную статью
     * @result array данные по парсингу заданной статьи
	 * @throws \Exception
	 * @return array
	 */
    private function parseArticle($url) {
        $r = new AsyncSocketRequest();
        if (!($text = $r->get($url))) {
            throw new \Exception("cannot fetch page " . $url);
        }
        $dom = str_get_html($text);
        list($guid, $title, $body, $pubTime) = array_fill(0, 4, '');
        
        if ($input = $dom->find("#commentform input#comment_post_ID", 0)) {
            $guid = intval($input->value);
        }
        if ($link = $dom->find('div.entry_post h1.maintitle a',0)) {
            $title = trim($link->plaintext);
            if (empty($title)) {
                $title = 'Без заголовка';
            }
        }
        if ($data = $dom->find('div.entry_post div.post',0)) {
            $body = trim(preg_replace('#<br\s*/?>#is', "\n", $data->plaintext));
        }
        
        if ($div = $dom->find("div.sidedateblock", 0)) {
            $day = intval(trim($div->plaintext));
            if(preg_match("#http://1001facts.info/(\d+)/(\d+)/#is", 
                           $this->currentPage['link'], 
                           $args)) {
                $year = intval($args[1]);                
                $month = intval($args[2]);
                $pubTime = mktime(0, 0, 0, $month, $day, $year);
            }
        }
        
        $categories = array();
        foreach ($dom->find('li.sidecategory') as $li) {
            if ($link = $li->find('a', 0)) {
                $categories[] = $link->plaintext;
            }
        }
        
        $images = array();
        foreach ($dom->find('img.aligncenter') as $image) {
            $images[] = $image->src;
        }
        
        if (empty($title) or empty($body) or empty($categories)) {
            throw new \Exception("cannot parse article page " . $url);            
        }
        $result = array('title' => $title, 'body' => $body, 
                        'guid' => $guid, 'pubTime' => $pubTime,
                        'categories' => $categories, 'images' => $images
                   );
        return $result;
    }

}
