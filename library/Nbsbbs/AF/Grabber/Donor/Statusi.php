<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 26.03.13
 * Time: 16:35
 */
namespace Nbsbbs\AF;

class Grabber_Donor_Statusi extends Grabber_Donor {

	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries() {
		$maxPage = $this->getMaxPages();
		$collection = $this->parsePage($this->getUrlByPageNum($maxPage));
		if ($maxPage > 2) $collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum($maxPage - 1)));
		return $collection;
	}

	/**
	 * Возвращает итератор со страницами для парсинга
	 *
	 * @return \Iterator
	 */
	public function getPagesIterator() {
		$collection = array();
		$maxPages = $this->getMaxPages(); // нужно, чтобы не делать запросы в цикле
		for ($i = $maxPages; $i >= 1; $i--) {
			$collection[] = $this->getUrlByPageNum($i);
		}
		return new \ArrayIterator($collection);
	}

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields() {
		return array();
	}

	/**
	 * @return int Число страниц у башорга
	 * @throws \Exception
	 */
	public function getMaxPages() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://statusi.su/")) {
			if (preg_match('| ... <a href="http://www.statusi.su/page/(.*)/">(.*)</a></span>|U', $text, $args)) {
				return intval($args[1]);
			} else {
				throw new \Exception("Не найден шаблон для текущей страницы statusi.su");
			}
		} else {
			throw new \Exception("Cannot fetch page http://statusi.su/");
		}
	}

	/**
	 * Возвращает список записей из строки
	 *
	 * @param string $text Текст страницы
	 * @return Bean_Grabber_Entry[]
	 */
	public function parseText($text) {
		$collection = array();
		foreach (explode('<div id="status">', $text) as $entry) {
			if (strpos($entry, '<div align=right>') !== false) {
				if (preg_match('|<div id="news-id-(.*)" style="display:inline;">(.*)</div>|U', $entry, $args)) {
					$guid = $args[1];
					$txt = iconv("cp1251", "utf-8", $args[2]);
					$txt = preg_replace("(<br.*?>)", "\r\n", $txt);
					$txt = htmlspecialchars_decode($txt);
					$title = "Статус #" . $guid;
					if (preg_match("#<div align=right>(.*)</div>#U", $entry, $args)) {
						$date = $args[1];
					}
					if (preg_match('|style="padding:0px 3px 0px 3px; font-size:14px; color:#ffffff; background:#00460d;" href="/(.*)/"|U', $text, $args)) {
						$category = $args[1];
					}
					$bean = new Bean_Grabber_Entry();
					//$bean = array();
					$bean->grabTime = time();
					$bean->guid = $guid;
					$bean->text = $txt;
					$bean->title = $title;
					$bean->pubTime = strtotime($date);
					# category
					if(isset($category)) {
						$catsModel = new Model_Donor_Categories();
						$catBean = new Bean_Donor_Category();
						$catBean->name = trim($category);
						$categoryId = $catsModel->getCategoryIdByName($this->donorId, $catBean->name);
						$catBean->id = $categoryId;
						$catBean->parentId = '0';
						$bean->categories[] = $catBean;
					}

					$collection[] = $bean;
				} else {
					echo "Strange piece\n";
					echo $entry . "\n\n\n\n";
				}
			}
		}
		return $collection;
	}

	/**
	 * Возвращает URL страницы по номеру
	 *
	 * @param int $pageNum
	 * @return string
	 */
	public function getUrlByPageNum($pageNum) {
		return "http://www.statusi.su/page/" . $pageNum . "/";
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
		$collection = array();
		foreach ($this->getPagesIterator() as $page) {
			try {
				$collection = array_merge($collection, $this->parsePage($page));
			} catch (\Exception $e) {
				echo "Exception: \n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}

	/**
	 * Возвращает список записей на конкретной странице
	 *
	 * @param string $page Bean_Grabber_Entry страницы
	 * @return Bean_Grabber_Entry[]
	 * @throws \Exception
	 */
	public function parsePage($page) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}
}