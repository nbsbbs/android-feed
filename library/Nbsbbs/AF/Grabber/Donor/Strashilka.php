<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 17:27
 */
namespace Nbsbbs\AF;

class Grabber_Donor_Strashilka extends Grabber_Donor {
	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries() {
		$collection = $this->parsePage($this->getUrlByPageNum(1));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(2)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(3)));
		return $collection;
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
		$collection = array();
		foreach ($this->getPagesIterator() as $page) {
			try {
				$collection = array_merge($collection, $this->parsePage($page));
			} catch (\Exception $e) {
				echo "Exception: \n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}

	/**
	 * Возвращает итератор со страницами для парсинга
	 *
	 * @return \Iterator
	 */
	public function getPagesIterator() {
		// cycle categories, then pages
		$collection = array();
		$maxPages = $this->getMaxPages();
		for ($i = 1; $i <= $maxPages; $i++) {
			$collection[] = $this->getUrlByPageNum($i);
		}
		return new \ArrayIterator($collection);
	}

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields() {
		return array();
	}

	/**
	 * Возвращает список записей из строки
	 *
	 * @param string $text Текст страницы
	 * @return Bean_Grabber_Entry[]
	 */
	public function parseText($text) {
		$catsModel = new Model_Donor_Categories();
		$collection = array();
		$dom = str_get_html($text);
		foreach ($dom->find("div.shortstory") as $story) {
			if ($date = $story->find("p.binfo font", 0)) {
				if (preg_match("#^(\d+)-(\d+)-(\d+), (\d+):(\d+)#si", $date->plaintext, $args)) {
					$bean = new Bean_Grabber_Entry();
					$bean->grabTime = time();
					$bean->pubTime = mktime($args[4], $args[5], 0, $args[2], $args[1], $args[3]);
					if ($link = $story->find("h3.btl a", 0)) {
						$bean->title = trim($link->plaintext);
						if (preg_match("#/(\d+)-([^/]+)\.html#si", $link->href, $args)) {
							$bean->guid = $args[1];
						} else continue;
						
					} else continue;
					if ($link = $story->find("p.argcat a", 0)) {
						$catBean = new Bean_Donor_Category();
						$catBean->name = trim($link->plaintext);
						$categoryId = $catsModel->getCategoryIdByName($this->donorId, $catBean->name);
						$categoryItem = $catsModel->getItemById($categoryId);
						$catBean->id = $categoryItem['id'];
						$catBean->parentId = $categoryItem['parent_id'];
						$bean->categories[] = $catBean;
					} else continue;
					if ($bean->text = $this->getStoryTextByUrl($story->find("h3.btl a", 0)->href)) {
						$collection[] = $bean;
					} else  continue;
				} else {
					echo "Bad date: ". $date->plaintext."\n";
				}
			} else {
				echo "No date";
			}
		}
		return $collection;
	}

	/**
	 * Возвращает текст истории по URL
	 *
	 * @param string $url URL Первой страницы
	 * @return string Текст истории
	 */
	public function getStoryTextByUrl($url) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($url)) {
			
			$dom = str_get_html($text);
			if ($text = $dom->find("div[id=cocont]", 0)) {
				return trim($text->plaintext);
			} else return "";
		} else {
			return "";
		}
	}

	/**
	 * Выдает массив категорий вида "название" => "URL"
	 *
	 * @throws \Exception
	 * @return string[] Массив доступных категорий "Название" => "URL";
	 */
	public function getCategories() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://strashilka.com/")) {
			$dom = str_get_html($text);
			$collection = array();
			foreach ($dom->find("div.leftmenu ul.lmenu li") as $element) {
				if ($link = $element->find("a", 0)) {
					if (trim($link->plaintext) != "Флудилка") $collection[trim($link->plaintext)] = trim($link->href);
				}
			}
			return $collection;
		} else {
			throw new \Exception("Cannot fetch http://strashilka.com/");
		}
	}

	/**
	 * Возвращает URL страницы по номеру
	 *
	 * @param int $pageNum
	 * @return string
	 */
	public function getUrlByPageNum($pageNum) {
		if ($pageNum == 1) {
			return "http://www.strashilka.com/";
		} else {
			return "http://www.strashilka.com/page/" . $pageNum."/";
		}
	}


	/**
	 * @return int Число страниц 
	 * @throws \Exception
	 */
	public function getMaxPages() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://strashilka.com/")) {
			$dom = str_get_html($text);
			if ($lastLink = $dom->find("div.basenavi div.navigation a")) {
				return $lastLink[sizeof($lastLink)-1]->plaintext;
			} else {
				throw new \Exception("Last page not found");
			}
		} else {
			throw new \Exception("Cannot fetch page http://strashilka.com/");
		}
	}

	/**
	 * Возвращает список записей на конкретной странице
	 *
	 * @param string $page Bean_Grabber_Entry страницы
	 * @return Bean_Grabber_Entry[]
	 * @throws \Exception
	 */
	public function parsePage($page) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}
}