<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 17:27
 */
namespace Nbsbbs\AF;

class Grabber_Donor_Pornorasskaz extends Grabber_Donor {
	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries() {
		$collection = $this->parsePage($this->getUrlByPageNum(1));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(2)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(3)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(4)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(5)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(6)));
		$collection = array_merge($collection, $this->parsePage($this->getUrlByPageNum(7)));
		return $collection;
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
		$collection = array();
		foreach ($this->getPagesIterator() as $page) {
			try {
				$collection = array_merge($collection, $this->parsePage($page));
			} catch (\Exception $e) {
				echo "Exception: \n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}

	/**
	 * Возвращает итератор со страницами для парсинга
	 *
	 * @return \Iterator
	 */
	public function getPagesIterator() {
		// cycle categories, then pages
		$collection = array();
		$maxPages = $this->getMaxPages();
		for ($i = 1; $i <= $maxPages; $i++) {
			$collection[] = $this->getUrlByPageNum($i);
		}
		return new \ArrayIterator($collection);
	}

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields() {
		return array();
	}

	/**
	 * Возвращает список записей из строки
	 *
	 * @param string $text Текст страницы
	 * @return Bean_Grabber_Entry[]
	 */
	public function parseText($text) {
		$catsModel = new Model_Donor_Categories();
		$collection = array();
		foreach (explode("<div class=\"post\">", $text) as $entry) {
			if ((strpos($entry, "<p class=\"category\">") !== false) and (strpos($entry, "<p class=\"date\">") !== false)) {
				if (preg_match("#<h2><a href=\"http://pornorasskaz\.com/sex/([^\"]+)_(\d+)\.html\">([^<]+)</a></h2>#siu", $entry, $args)) {
					$bean = new Bean_Grabber_Entry();
					$bean->grabTime = time();
					$bean->guid = $args[2];
					$bean->title = $args[3];
					if (preg_match("#<p class=\"date\"><em>(\d+)/(\d+)/(\d+)</em></p>#si", $entry, $argss)) {
						$bean->pubTime = mktime(date("H"), date("i"), 0, $argss[1], $argss[2], "20" . $argss[3]);
					} else {
						continue;
					}
					if (preg_match("#<p class=\"category\">Категории: (.+?)<p>#sui", $entry, $cats)) {

						preg_match_all("#<a href=\"[^\"]+?\" title=\"[^\"]+?\" rel=\"[^\"]+?\">([^<]+?)</a>#sui", $cats[1], $catss, PREG_SET_ORDER);
						foreach ($catss as $cat) {
							$catBean = new Bean_Donor_Category();
							$categoryId = $catsModel->getCategoryIdByName($this->donorId, preg_replace("#::\s+#siu", " ", $cat[1]));
							$categoryItem = $catsModel->getItemById($categoryId);
							$catBean->id = $categoryItem['id'];
							$catBean->name = $categoryItem['category_name'];
							$catBean->parentId = $categoryItem['parent_id'];
							$bean->categories[] = $catBean;
						}
					}
					if ($bean->text = $this->getStoryTextByUrl("http://pornorasskaz.com/sex/" . $args[1] . "_" . $args[2] . ".html")) {
						$collection[] = $bean;
					} else {
						continue;
					}
				}
			}
		}
		return $collection;
	}

	/**
	 * Возвращает текст истории по URL
	 *
	 * @param string $url URL Первой страницы
	 * @return string Текст истории
	 */
	public function getStoryTextByUrl($url) {
		echo __FUNCTION__ . ": " . $url . "\n";
		$rq = new AsyncSocketRequest();
		$text = $rq->get($url);

		if (preg_match("#<div class=\"text\">(.+?)<p><strong>Страница:</strong>(.+?)\">(\d+)</a></p>\s+</div>#sui", $text, $args)) {
			if ($args[3] == 1) {
				return $args[1];
			} elseif ($args[3] == 0) {
				return false;
			} else {
				$ret = trim($args[1]);
				for ($i = 2; $i <= $args[3]; $i++) {
					echo "Fetch " . $url . "/" . $i . "\n";
					$txt = $rq->get($url . "/" . $i);
					if (preg_match("#<div class=\"text\">(.+?)<p><strong>Страница:</strong>#sui", $txt, $argss)) {
						$ret = $ret . " " . trim($argss[1]);
					}
				}
				return $ret;
			}
		} else {
			return false;
		}
	}

	/**
	 * Выдает массив категорий вида "название" => "URL"
	 *
	 * @throws \Exception
	 * @return string[] Массив доступных категорий
	 */
	public function getCategories() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://pornorasskaz.com/")) {
			if (preg_match("#<div class=\"block\">\s*<h3>Категории</h3>\s*<ul>(.+?)</ul>#siu", $text, $args)) {
				preg_match_all("#<li class=\"cat-item cat-item-\d+\"><a href=\"([^\"]+)\" title=\"[^\"]*\">(.+?)</a>#siu", $args[1], $args, PREG_SET_ORDER);
				$collection = array();
				for ($i = 0; $i < sizeof($args); $i++) {
					$args[$i][2] = preg_replace("#::\s+#siu", " ", $args[$i][2]);
					$collection[$args[$i][2]] = $args[$i][1];
				}
				return $collection;
			} else {
				throw new \Exception("Не найден паттерн категорий");
			}
		} else {
			throw new \Exception("Cannot fetch http://pornorasskaz.com/");
		}
	}

	/**
	 * Выдает число страниц в категории по ее URL
	 *
	 * @param string $categoryUrl URL категории
	 * @return int Число страниц в категории
	 * @throws \Exception
	 */
	public function getPagesInCategory($categoryUrl) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($categoryUrl)) {
			if (preg_match("#<a href=\"http://pornorasskaz\.com/.+?/page/(\d+)\" title=\"последняя \&raquo;#siu", $text, $args)) {
				return intval($args[1]);
			} else {
				return 1;
			}
		} else {
			throw new \Exception("Cannot fetch {$categoryUrl}");
		}
	}


	/**
	 * Возвращает URL страницы по номеру
	 *
	 * @param int $pageNum
	 * @return string
	 */
	public function getUrlByPageNum($pageNum) {
		if ($pageNum == 1) {
			return "http://pornorasskaz.com/";
		} else {
			return "http://pornorasskaz.com/page/" . $pageNum;
		}
	}


	/**
	 * @return int Число страниц у порнорассказа
	 * @throws \Exception
	 */
	public function getMaxPages() {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get("http://pornorasskaz.com/")) {
			if (preg_match("#<a href=\"http://pornorasskaz.com/page/(\d+)\" title=\"последняя#siu", $text, $args)) {
				return intval($args[1]);
			} else {
				throw new \Exception("Не найден шаблон для текущей страницы pornorasskaz.com");
			}
		} else {
			throw new \Exception("Cannot fetch page http://pornorasskaz.com/");
		}
	}

	/**
	 * Возвращает список записей на конкретной странице
	 *
	 * @param string $page Bean_Grabber_Entry страницы
	 * @return Bean_Grabber_Entry[]
	 * @throws \Exception
	 */
	public function parsePage($page) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}

}