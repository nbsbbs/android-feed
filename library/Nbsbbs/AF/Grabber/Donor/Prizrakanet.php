<?php
/**
 * Created by JetBrains PhpStorm.
 * User: zolt
 * Date: 26.03.13
 * Time: 14:21
 */

namespace Nbsbbs\AF;


class Grabber_Donor_Prizrakanet extends Grabber_Donor {

	private $currentLink;
	private $currentCategory;

	public function __construct(){
		$this->currentLink = '';
		$this->currentCategory = array();
	}

	public function getAllEntries () {
		$collection = array();
		foreach($this->getCategories() as $category){
			$this->currentCategory = $category;
			$collection = array_merge($collection, $this->parseCategory($category[1]));
		}
		return $collection;
	}

	private function parseCategory($link){
		$result = array();
		foreach($this->getStoryLinksFromCategory($link) as $storyLink){
			$this->currentLink = $storyLink;
			$result = array_merge($result, $this->parsePage($storyLink));
		}
		return $result;
	}

	private function getStoryLinksFromCategory($link) {
		$rq = new AsyncSocketRequest();
		if($storypage = $rq->get($link)){
			preg_match_all('/(?<=<li><a href=\')(\S+)(?=\'>)/', $storypage, $links);
			return $links[0];
		} else {
			throw new \Exception("Can't fetch story links\n
                                    for category[".array_search($this->currentCategory, $link)."]\n
                                    by link: ".$link);
		}
	}


	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getNewEntries () {
		$collection = array();
		return $collection;
		$links = $this->getNewLinks();
		foreach($links as $link){
			$this->currentLink = $link;
			$collection = array_merge($collection, $this->parsePage($link));
		}
		return $collection;
	}

	private function getNewLinks(){
		$rq = new AsyncSocketRequest();
		if($text = $rq->get('http://prizraka.net/')){
			preg_match_all('/(?<=<li><a href=\')(\S+)(?=\'>)/', $text, $links);
			return $links[0];
		} else {
			throw new \Exception("Can't get new links from prizraka.net");
		}
	}

	/**
	 * Возвращает итератор со страницами для парсинга
	 *
	 * @throws \Exception если не определены категории
	 * @return \Iterator
	 */
	public function getPagesIterator () {
		$collection = array();
		foreach ($this->getCategories() as $link) {
			$collection[] = $link;
		}
		return new \ArrayIterator($collection);
	}

	/**
	 * Возвращает список дополнительных параметров
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields () {
		return array();
	}

	/**
	 * Возвращает список записей из строки
	 *
	 * @param string $text Текст страницы
	 * @throws \Exception
	 * @return Bean_Grabber_Entry[]
	 */
	public function parseText ($text) {
		$result = array();
		$entry = new Bean_Grabber_Entry();
		$entry->grabTime = time();
		$entry->pubTime = $entry->grabTime;
		$entry->guid = $this->currentLink;
		if (!isset($this->currentCategory)) {
			$entry->categories[] = new Bean_Donor_Category();
		} else {
			$model = new Model_Donor_Categories();
			$category = new Bean_Donor_Category();
			$category->id = $model->getCategoryIdByName($this->donorId, $this->currentCategory[0]);
			$category->name = $this->currentCategory[0];
			$entry->categories[] = $category;
		}
		preg_match('/(?<=<h1>\s)(.*)(?=<\/h1>)/', $text, $title);
		$entry->title = iconv("CP1251", "UTF-8", trim($title[0]));
		preg_match('/(?<=<\/h1>\s)(.*)(?=\s)/', $text, $textt);
		$entry->text = iconv("CP1251", "UTF-8", strip_tags(str_replace('&nbsp;', '', trim($textt[0]))));
		$result[] = $entry;
		return $result;

	}

	/**
	 * Получим все категории
	 * @return array
	 * @throws \Exception
	 */
	public function getCategories() {
		$rq = new AsyncSocketRequest();
		$result = array();
		if($text = $rq->get('http://prizraka.net/')) {
			$dom = str_get_html($text);
			if($categories = $dom->find('table td[bgcolor=#220022] a[href^=http://prizraka.net]')) {
				foreach($categories as $item){
					if($item->href === 'http://prizraka.net/index.html' || $item->href === 'http://prizraka.net/news.rss'){
						continue;
					}
					array_push($result, array(0=>iconv("CP1251", "UTF-8", trim($item->innertext)), 1=>iconv("CP1251", "UTF-8",trim($item->href))));
				}
				return $result;
			} else {
				throw new \Exception("Can't get categories links");
			}
		} else {
			throw new \Exception("Can't fetch page http://prizraka.net/");
		}
	}

	public function parsePage ($page) {
		$rq = new AsyncSocketRequest();
		$rq->setTimeout(2000);
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}

}