<?php

/**
 * @author tvolf
 * Date: 26.03.13
 * Time: 11:55
 */

namespace Nbsbbs\AF;

class Grabber_Donor_Statusisu extends Grabber_Donor {

	private $categoriesList = array();

	public function getAdditionalFields() {
		return array();
	}

	/**
	 * Возвращает массив всех записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	public function getAllEntries() {
		$collection = array();
		foreach ($this->getPagesIterator() as $page) {
			try {
				$collection = array_merge($collection, $this->parsePage($page));
			} catch (\Exception $e) {
				echo "Exception: \n".$e->getTraceAsString()."\n";
			}
		}
		return $collection;
	}

	public function getPagesIterator() {
		if (empty($this->categoriesList)) {
			$this->categoriesList = $this->getCategoriesList();
		}
		$result = array();
		foreach ($this->categoriesList as $category) {
			$maxNum = $this->getMaxPage($category['link']);
//                echo 'MaxNum for categories ' . $category['title'] . ' = ' . $maxNum . '<br/>';
			for ($i = $maxNum; $i > 0; --$i) {
				$result[] = $this->getUrlById($category['link'], $i);
			}
		}
//          print_r($result);
//          die();
		return $result;
	}

	public function getNewEntries() {
		if (empty($this->categoriesList)) {
			$this->categoriesList = $this->getCategoriesList();
		}
		$r = new AsyncSocketRequest();
		$block = array();
		foreach ($this->categoriesList as $category) {
			$maxPage = $this->getMaxPage($category['link']);
			$minPage = $maxPage - 2 > 0 ? $maxPage - 2 : 1;
			for ($i = $maxPage; $i > $minPage; --$i) {
				if ($text = $r->get($this->getUrlById('http://statusi.su/', $i))) {
					$block = array_merge($block, $this->parseText($text));
				} else {
					throw new \Exception("Cannot fetch page " . $this->getUrlById('http://statusi.su/', $i));
				}
			}
		}
		return $block;
	}

	/**
	 * Получение списка всех категорий сайта statusi.su кроме Главной
	 * @return array список все категорий сайта кроме Главной
	 * @throws \Exception
	 */
	private function getCategoriesList() {
		$baseUrl = 'http://www.statusi.su';
		$categoriesList = array();
		$r = new AsyncSocketRequest();
		$text = $r->get($baseUrl . '/');
		if ($text == FALSE) {
			throw new \Exception("Cannot fetch page http://www.statusi.su/");
		}
		$dom = str_get_html($text);
		if ($arr = $dom->find("div#left a")) {
			foreach ($arr as $link) {
				$categoriesList[] = array(
					'link' => $baseUrl . $link->href,
					'title' => $link->plaintext
				);
			}
		} else {
			throw new \Exception("Cannot get categories from page http://www.statusi.su/");
		}

		$arr = explode('<h1>', $text);
		if (count($arr) > 1) {
			$last = $arr[1];
			$cnt = preg_match_all("#<li>[^<]*<a href=(.*?)>(.*?)</a>#is", $last, $arr2, PREG_SET_ORDER);
			foreach ($arr2 as $elem) {
				$categoriesList[] = array(
					'link' => $elem[1],
					'title' => iconv('windows-1251', 'utf-8', $elem[2])
				);
			}
		}

		unset($categoriesList[0]); // удаляем "Главную", чтобы её не обрабатывать вообще
		return $categoriesList;
	}

	/**
	 * Функция возвращает максимальный номер страницы для заданной категории сайта statusi.su
	 * @param  string $baseLink     базовый url категории
	 * @return int  Максимальный номер страницы сайта заданной категории
	 * @throws \Exception
	 */
	private function getMaxPage($baseLink) {
		$r = new AsyncSocketRequest();
		$maxPage = -1;
		if ($text = $r->get($baseLink)) {
			$dom = str_get_html($text);
			if ($arr = $dom->find("#dle-speedbar a[href^=" . $baseLink . "page/]")) {
				$maxPage = intval($arr[count($arr) - 1]->plaintext);
			}
			if ($maxPage == -1) {
				throw new \Exception("Cannot find max page number for " . $baseLink);
			}
		} else {
			throw new \Exception("Cannot fetch page " . $baseLink);
		}
		return $maxPage;
	}

	/**
	 * Функция возвращает url страницы по её категории и id
	 * @param string $baseUrl базовый url категории
	 * @param int $id    id страницы
	 * @return string    url страницы
	 */
	private function getUrlById($baseUrl, $id) {
		if ($id == 1) {
			$url = $baseUrl;
		} else {
			$url = $baseUrl . "page/" . $id . "/";
		}
		return $url;
	}

	public function parseText($text) {
		$result = array();
//          echo htmlspecialchars($text). '<br><br>';
		$categoryName = 'Без категории';
		if (preg_match('#<p>[^<]*<span id=\'dle-speedbar\'>[^<]*' .
			'<a href="http://www.statusi.su/">[^<]*</a>[^<]*' .
			'<a href="http://www.statusi.su/(.*?)/">(.*?)</a></span>#is', $text, $args)
		) {
			$categoryName = iconv('windows-1251', 'utf-8', trim($args[2]));
		}
//          echo $categoryName . '<br/>';          
//          die();
		$catsModel = new Model_Donor_Categories();
		$arr = explode('<div id="cont">', $text);
		foreach ($arr as $block) {
			$id = 0;
			$body = '';
			$time = 0;
			if (preg_match(
				'#<div id="status">[^<]*<div id="news-id-(\d+)" style="display:inline;">' .
					'(.*?)</div>[^<]*</div>[^<]*<div id=\'ratig-layer-(\d+)\'>#is', $block, $args)
			) {
				$id = intval($args[1]);
				$body = trim($args[2]);
			}
			if (preg_match('#<div align=right>(\d+)-(\d+)-(\d+)</div>#is', $block, $args)) {
				$time = mktime(0, 0, 0, intval($args[2]), intval($args[1]), intval($args[3]));
//                  echo '$time = "' . $time . '"<br/>';                                    
			}
			if (!empty($id) && !empty($time) && !empty($body)) {
				$bean = new Bean_Grabber_Entry();
				$bean->grabTime = time();
				$bean->text = iconv('windows-1251', 'utf-8', $body);
				$bean->guid = $id;
				$bean->title = 'Статус ' . $bean->guid;
				$bean->pubTime = $time;

				$catBean = new Bean_Donor_Category();
				$catBean->name = trim($categoryName);
				$categoryId = $catsModel->getCategoryIdByName($this->donorId, $catBean->name);
				$categoryItem = $catsModel->getItemById($categoryId);
				$catBean->id = $categoryItem['id'];
				$catBean->parentId = $categoryItem['parent_id'];
				$bean->categories[] = $catBean;

				$result[] = $bean;
			}
		}
		return $result;
	}

	/**
	 * Возвращает список записей на конкретной странице
	 *
	 * @param string $page Bean_Grabber_Entry страницы
	 * @return Bean_Grabber_Entry[]
	 * @throws \Exception
	 */
	public function parsePage($page) {
		$rq = new AsyncSocketRequest();
		if ($text = $rq->get($page)) {
			return $this->parseText($text);
		} else {
			throw new \Exception("Cannot fetch page " . $page);
		}
	}

}
