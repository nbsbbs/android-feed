<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 15:38
 */
namespace Nbsbbs\AF;

class Grabber_Factory {

	/**
	 * Выдает экземпляр класса типа Grabber_Donor
	 *
	 * @param Grabber_Donor|string $grabber
	 * @return Grabber_Donor
	 * @throws \Exception
	 */
	public function getGrabber($grabber) {
		if (is_object($grabber) and ($grabber instanceof Grabber_Donor)) {
			return $grabber;
		} else {
			$className = __NAMESPACE__ . "\\Grabber_Donor_" . ucfirst($grabber);
			if (class_exists(__NAMESPACE__ . "\\" . $grabber)) {
				$clName = __NAMESPACE__ . "\\" . $grabber;
				return new $clName;
			} elseif (class_exists($className)) {
				return new $className;
			} else {
				throw new \Exception("Класс {$className} не найден");
			}
		}
	}
}