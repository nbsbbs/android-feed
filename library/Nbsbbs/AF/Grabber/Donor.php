<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 06.03.13
 * Time: 14:14
 */
namespace Nbsbbs\AF;

abstract class Grabber_Donor implements IFace_Grabber {
	/**
	 * @var int ID Донора [optional]
	 */
	public $donorId = 0;

	/**
	 * Возвращает массив новых записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	abstract public function getNewEntries();

	/**
	 * Возвращает массив всех доступных записей
	 *
	 * @return Bean_Grabber_Entry[]
	 */
	abstract public function getAllEntries();

	/**
	 * Задать $donorId
	 * 
	 * @param int $id ID донора (af_donors)
	 */
	public function setDonorId($id) {
		$this->donorId = $id;
	}

	/**
	 * Возвращает список дополнительных параметров
	 * 
	 * Дополнительные параметры это, например, картинки, если они прикреплены к постам.
	 *
	 * @return Form_Field[] Дополнительные поля для данного типа
	 */
	public function getAdditionalFields() {
		return array();
	}

}