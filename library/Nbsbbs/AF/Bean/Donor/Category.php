<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 14.03.13
 * Time: 22:10
 */
namespace Nbsbbs\AF;

class Bean_Donor_Category {
	/**
	 * @var string Отображаемое название категории
	 */
	public $name;
	/**
	 * @var int ID категории в таблице af_donors_categories
	 */
	public $id;
	/**
	 * @var int [optional] ID родительской категории в таблице af_donors_categories 
	 */ 
	public $parentId = 0;
}