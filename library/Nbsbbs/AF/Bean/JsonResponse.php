<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 11.03.13
 * Time: 19:10
 */
namespace Nbsbbs\AF;

class Bean_JsonResponse {
	/**
	 * @var string Версия API
	 */
	public $version = "1.0";
	/**
	 * @var bool
	 */
	public $status;
	/**
	 * @var string Описание ошибки
	 */
	public $error;
	/**
	 * @var array Тело ответа
	 */
	public $response;

	/**
	 * @return string JSON-представление
	 */
	public function __toString() {
		return json_encode($this, JSON_HEX_QUOT);
	}

}