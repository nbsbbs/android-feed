<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 02.03.13
 * Time: 21:13
 */
namespace Nbsbbs\AF;

class Bean_Grabber_Entry {
	/**
	 * @var Bean_Donor_Category[] Категории (optional)
	 */
	public $categories = array();
	/**
	 * @var string Заголовок записи (optional)
	 */
	public $title;
	/**
	 * @var string Текст записи
	 */
	public $text;
	/**
	 * @var int Время публикации (optional)
	 */
	public $pubTime;
	/**
	 * @var array Дополнительные параметры (optional)
	 */
	public $additionalParams = array();
	/**
	 * @var int Время граба
	 */
	public $grabTime;
	/**
	 * @var string GUID
	 */
	public $guid;

	/**
	 * Делает строку из сохраненных дополнительных параметров
	 *
	 * @return string
	 * @throws \Exception Ошибка json_encode
	 */
	public function paramsToString() {
		$string = json_encode($this->additionalParams);
		if (($errorCode = json_last_error()) != JSON_ERROR_NONE) {
			throw new \Exception("json_encode failed, code " . $errorCode, 500);
		}
		return $string;
	}

	/**
	 * Сохраняет дополнительные параметры из строки
	 *
	 * @param $string
	 * @throws \Exception Ошибка json_decode
	 */
	public function stringToParams($string) {
		$this->additionalParams = json_decode($string, 1);
		if (($errorCode = json_last_error()) != JSON_ERROR_NONE) {
			throw new \Exception("json_decode failed, code " . $errorCode, 500);
		}
	}

	public function __construct() {
		$this->grabTime = time();
	}
}