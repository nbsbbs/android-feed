<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:04
 */
namespace Nbsbbs\Db;

class LazyPDO {
	private $dsn;
	private $login;
	private $pass;
	private $params;
	private $db;
	private $reconnectLevel = 0;

	/**
	 * @param string $dsn
	 * @param string $login
	 * @param string $pass
	 * @param array $params
	 */
	public function __construct($dsn, $login, $pass, $params) {
		$this->dsn = $dsn;
		$this->login = $login;
		$this->pass = $pass;
		$this->params = $params;
		return $this;
	}
	
	public function disconnect() {
		unset($this->db);
	}

	public function __call($name, $args) {
		$this->connect();
		try {
			$result = call_user_func_array(array($this->db, $name), $args);
			return $result;
		} catch (\Exception $e) {
			if ($this->reconnectLevel < 1) {
				// здесь код PDOException универсальный, поэтому проверяем message
				if (preg_match("#MySQL server has gone away#si", $e->getMessage())) {
					$this->disconnect(); // reconnect
					$this->reconnectLevel += 1;
					echo "Reconnect!\n";
					try {
						$result = $this->__call($name, $args);
						$this->reconnectLevel = 0;
						return $result;
					} catch (\Exception $e) {
						throw $e;
					}
				} else {
					throw $e;
				}
			} else {
				throw $e;
			}
		}
	}

	/**
	 *
	 * @return \PDO
	 */
	public function connect() {
		if (!$this->db) {
			$this->db = new \PDO(
				$this->dsn,
				$this->login,
				$this->pass,
				$this->params
			);
			$this->db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
			$this->db->exec("SET NAMES 'utf8'");
			$this->db->exec("SET wait_timeout=10");
			$this->db->setAttribute(\PDO::ATTR_PERSISTENT, true);
			$this->db->exec("SET time_zone = '+04:00';");
			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}
	}

}
