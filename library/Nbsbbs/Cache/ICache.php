<?php
namespace Nbsbbs\Cache;

interface ICache {
	public function get($key, $ignoreTtl = false);

	public function set($key, $value, $ttl);

	public function del($key);
}