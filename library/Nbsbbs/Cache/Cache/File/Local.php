<?php
namespace Nbsbbs\Cache;

class Cache_File_Local implements ICache {
	private $registry, $basePath;

	public function __construct() {
		$this->basePath = PATHS_FILECACHE;
	}

	public function get($key, $ignoreTtl = false) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		if (file_exists($this->getFilenameByKey($key))) {
			$data = unserialize(gzinflate(file_get_contents($this->getFilenameByKey($key), FILE_BINARY)));
			if (!is_array($data)) {
				return false;
			}
			if ($ignoreTtl) return $data['data'];
			if ($data['expire'] > time()) return $data['data'];
			return false;
		} else return false;
	}

	public function set($key, $value, $ttl) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		$newValue = array("key" => $key, "expire" => time() + $ttl, "data" => $value);
		if (!is_writable(dirname($this->getFilenameByKey($key)))) {
			if (!mkdir(dirname($this->getFilenameByKey($key)), 0777, true)) {
				throw new \Exception("Cannot create directory " . dirname($this->getFilenameByKey($key)), 500);
			}
		}
		$r = file_put_contents($this->getFilenameByKey($key), gzdeflate(serialize($newValue)), FILE_BINARY);
		chmod($this->getFilenameByKey($key), 0666);
		return $r;
	}

	public function del($key) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		if (file_exists($this->getFilenameByKey($key))) {
			if (!unlink($this->getFilenameByKey($key))) {
				throw new \Exception("Cannot delete local cache file '{$this->getFilenameByKey($key)}'", 500);
			}
			return true;
		} else return true;
	}

	private function getFilenameByKey($key) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		return $this->basePath . $this->generateFilename($key);
	}

	private function generateFilename($key) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		$hash = md5($key);
		return "/" . substr($hash, 0, 2) . "/" . substr($hash, 2, 1) . "/" . $hash . ".cache";
	}

	public function chmodAll() {
		return $this->basePath;
		return false;
		exec("rm -rf " . $this->basePath, $out);
		var_dump($out);
		return $out;
	}

	public function clearAll() {
		return false;
		exec("rm -rf " . $this->basePath . "/*", $out);
		return $out;
	}
}