<?php
namespace Nbsbbs\Cache;

class Cache_File implements ICache {
	private $type = 'local';
	private $item, $registry;

	public function __construct($type = "local") {
		$this->type = $type;
		if ($this->type == "local") {
			$this->item = new Cache_File_Local();
		} else {
			throw new \Exception("Bad file cache type '{$type}'", 500);
		}
	}

	public function get($key, $ignoreTtl = false) {
		return $this->item->get($key, $ignoreTtl);
	}

	public function set($key, $value, $ttl) {
		return $this->item->set($key, $value, $ttl);
	}

	public function del($key) {
		return $this->item->del($key);
	}
}