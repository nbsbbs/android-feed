<?php
namespace Nbsbbs\Cache;

class Cache_Memcache implements ICache {
	/**
	 * @var string local, remote
	 */
	private $type = 'local';
	private $item;

	/**
	 * @param string $type
	 * @throws \Exception
	 */
	public function __construct($type = "local") {
		$this->type = $type;
		if ($this->type == "local") {
			$this->item = new Cache_Memcache_Local();
		} else {
			throw new \Exception("Bad memcache cache type '{$type}'", 500);
		}
	}

	/**
	 * @param $key
	 * @param bool $ignoreTtl
	 * @return mixed
	 */
	public function get($key, $ignoreTtl = false) {
		return $this->item->get($key, $ignoreTtl);
	}

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param integer $ttl Время жизни в секундах
	 * @return mixed
	 */
	public function set($key, $value, $ttl) {
		return $this->item->set($key, $value, $ttl);
	}

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function del($key) {
		return $this->item->del($key);
	}
}