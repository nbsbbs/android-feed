<?php
namespace Nbsbbs\Cache;

class Cache_Manager {
	static $items = array();

	/**
	 * @param $type
	 * @param string $location Строка local
	 * @return ICache Обработчик кеша
	 * @throws \Exception
	 */
	public static function getInstance($type, $location) {
		if (!empty(self::$items[$type . "_" . $location])) return self::$items[$type . "_" . $location];
		if (!class_exists($type)) throw new \Exception("Cache type not exists", 500);
		return (self::$items[$type . "_" . $location] = new $type($location));
	}
}