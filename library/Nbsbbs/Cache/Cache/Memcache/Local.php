<?php
namespace Nbsbbs\Cache;

class Cache_Memcache_Local implements ICache {
	/**
	 * @var \Memcache
	 */
	private $handler = null;
	private $prefix = "";

	public function __construct() {
		$this->getHandler();
		$this->prefix = MAIN_DOMAIN;
	}

	public function getHandler() {
		$this->handler = new \Memcache();
		if ($this->handler->connect(MEMCACHE_HOST, MEMCACHE_PORT)) {
			// все ок
		} else throw new \Exception("Cannot connect to memcache", 500);
	}

	public function get($key, $ignoreTtl = false) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		if ($result = $this->handler->get($this->getNameByKey($key))) {
			$data = unserialize(gzinflate($result));
			if (!is_array($data)) {
				return false;
			}
			if ($ignoreTtl) return $data['data'];
			if ($data['expire'] > time()) return $data['data'];
			return false;
		} else {
			return false;
		}
	}

	public function set($key, $value, $ttl) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		$newValue = array("expire" => time() + $ttl, "data" => $value);
		if ($this->handler->set($this->getNameByKey($key), gzdeflate(serialize($newValue)), false, time() + $ttl)) {
			//
		} else return false;
	}

	public function del($key) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		$this->handler->delete($this->getNameByKey($key));
	}

	private function getNameByKey($key) {
		if (empty($key)) throw new \Exception("Empty key given", 500);
		if (function_exists("mb_strtolower")) {
			$key = mb_strtolower($key, "UTF-8");
		} else {
			$key = strtolower($key);
		}
		$hash = md5($key);
		return $this->prefix . '_' . $hash;
	}
}