<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Noobus
 * Date: 04.03.13
 * Time: 17:11
 */
date_default_timezone_set("Europe/Moscow");
ini_set("display_errors", 1);
error_reporting(E_ALL & ~E_NOTICE);

require("library/config.php");

$registry->db = new \Nbsbbs\Db\LazyPDO("mysql:host=" . $registry->mysqlHost . ";dbname=" . $registry->mysqlDbName, $registry->mysqlUser, $registry->mysqlPassword, array());
$router = new \Nbsbbs\AF\Router();

try {
	$grabber = new \Nbsbbs\AF\Grabber_Donor_Prizrakanet();
	var_dump($grabber->getAllEntries());
} catch (Exception $e) {
	var_dump($e->getMessage());
	var_dump($e->getCode());
	echo $e->getTraceAsString() . "\n";
}   