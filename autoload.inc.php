<?php

function replaceUScoreAndBSlashes($class_name, $prefix = '') {
    return preg_replace("#(_|\\\\)#s", 
            DIRECTORY_SEPARATOR , 
            $prefix . $class_name);    
}


function getClassFileName(array $components) {
    return implode( DIRECTORY_SEPARATOR, $components) . '.php';
}

function nbsbbsAutoload($class_name) {
    $path = getClassFileName(
            array(
                __DIR__, "library", replaceUScoreAndBSlashes($class_name)    
            ));
    $result = include $path;
    if (!file_exists($path)) {
        $pathLib = getClassFileName(
            array(
                __DIR__,  "library",  "Nbsbbs",  "AF", 
                replaceUScoreAndBSlashes($class_name, DIRECTORY_SEPARATOR)
            )
        );
        include_once($pathLib);
    }
}

spl_autoload_register("nbsbbsAutoload");


