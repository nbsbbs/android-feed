{extends template="layout"}

{block name="head_title"}<?= (!empty($fields['id']->currentValue)) ? ("Редактировать") : ("Добавить") ?> пост донора{/block}

{block name="body_content"}
<h2>Доноры: посты</h2>

<form class="form" method="POST" action="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/add/">
    <fieldset>
        <legend>
			<?
			if (!empty($fields['id']->currentValue)) {
				?>
                Редактирование поста донора <?= $item['id'] ?>
				<?
			} else {
				?>
                Добавление нового поста донора
				<?
			}
			?>
        </legend>
		<?
		if (!empty($errors)) {
			foreach ($errors as $error) {
				?>
                <div class="alert">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Ошибка!</strong> <BR> <?=$error?>
                </div>
				<?
			}
		}
		foreach ($fields as $field) {
			if (empty($fields['id']->currentValue)) {
				if (!$field->isAddHidden) echo $field->render("form");
			} else {
				echo $field->render("form");
			}
		}
		?>
        <button type="submit"
                class="btn"><?=(!empty($fields['id']->currentValue)) ? ("Редактировать") : ("Добавить")?></button>
        <a class="btn" href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/">Вернуться</a>
    </fieldset>
    <input type="hidden" name="method" value="create">

</form>
{/block}