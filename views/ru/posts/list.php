{extends template="layout"}

{block name="head_title"}Просмотр постов доноров, страница <?= $page ?> из <?= $totalPages ?>{/block}

{block name="body_content"}
<h2>Доноры: посты</h2>

<!-- пагинация -->
<div class="pagination pagination-centered pagination-large">
    <ul>
		<?
		$minPage = max(1, $page-6);
		$maxPage = min($page+6, $totalPages);
		if ($minPage > 1) {
			?>
			<li>
				<a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/list/1/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>">1</a>
			</li>
			<?
			if ($minPage > 2) {
				?>
				<li class="active">
					<a href="">..</a>
				</li>
			<?
			}
		}
		for ($i = $minPage; $i <= $maxPage; $i++) {
			if ($i == $page) {
				?>
                <li class="active"><a
                        href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/list/<?=$i?>/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>"><?=$i?></a>
                </li>
				<?
			} else {
				?>
                <li>
                    <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/list/<?=$i?>/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>"><?=$i?></a>
                </li>
				<?
			}
		}
		if ($maxPage < $totalPages) {
			if ($maxPage < ($totalPages - 1)) {
				?>
				<li class="active">
					<a href="">..</a>
				</li>
				<?
			}
			?>
			<li>
				<a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/list/<?=$totalPages?>/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>"><?=$totalPages?></a>
			</li>
		<?
		}
		?>
    </ul>
</div>

<?
if (empty($items)) {
	?>
<div class="alert alert-info">Тут пока пусто.</div>
<?
} else {
	?>
<table class="table table-hover">
    <thead>
    <tr>
		<?
		foreach ($fields as $field) {
			if (!$field->isListHidden) {
				?>
                <th><?=$field->displayName?></th>
				<?
			}
		}
		?>
        <th>Действия</th>
    </tr>
    </thead>
    <tbody>
		<?
		foreach ($items as $item) {
			?>
        <tr>
			<?
			foreach ($fields as $field) {
				if (!$field->isListHidden) {
					if ($field instanceof \Nbsbbs\AF\IFace_SeparateData) {
						$field->currentValue = $field->loadSData($item['id']);
					} else {
						$field->currentValue = $field->convertDbFormatToValue($item[$field->fieldName]);
					}
					?>
                    <td><?=$field->render("list")?></td>
					<?
				}
			}
			?>
            <td>
                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/edit/<?=$item['id']?>"><i
                        class="icon-pencil" title="редактировать"></i></a>

                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/delete/<?=$item['id']?>"
                   style="margin-left: 20px"><i class="icon-remove" title="удалить"></i></a>
            </td>
        </tr>
			<?
		}
		?>
    </tbody>
</table>
<?
}
?>
{/block}