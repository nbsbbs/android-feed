<!DOCTYPE html>
<html>
<head>
    <title>{block name="head_title"}Android Feed{/block}</title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/jquery.gritter.css">
    <link rel="stylesheet" href="/css/jquery.lightbox-0.5.css">
    <script language="javascript" src="/js/jquery.gritter.js"></script>
    <script language="javascript" src="/js/jquery.lightbox-0.5.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="navbar navbar-static-top">
    <div class="navbar-inner">
        <a class="brand" href="#">Android Feed</a>
        <ul class="nav">
            <li <?=($controller == "Controller_Index") ? ("class='active'") : ("")?>><a href="/">Начало</a></li>
            </li>
            <li class="dropdown<?=($controller == "Controller_Donors") ? (" active") : ("")?>">
                <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" id="dropDonors">Доноры<b
                        class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropDonors">
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/">Список</a></li>
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/add/">Добавить
                        новый</a></li>
                </ul>
            </li>
            <li class="dropdown<?=($controller == "Controller_Posts") ? (" active") : ("")?>">
                <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" id="dropPosts">Посты доноров<b
                        class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropPosts">
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/">Список</a></li>
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/add/">Добавить
                        новый</a></li>
                </ul>
            </li>
            <!-- <li class="dropdown<?=($controller == "Controller_Feeds") ? (" active") : ("")?>">
                <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" id="dropFeeds">Фиды<b
                        class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropFeeds">
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/feeds/">Список</a></li>
                    <li><a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/feeds/add/">Добавить новый</a>
                    </li>
                </ul>
            </li> -->
        </ul>
    </div>
</div>
<div class="container">
    {block name="body_content"}Hey!Hey! Title page!{/block}
</div>
</body>
</html>