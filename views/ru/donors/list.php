{extends template="layout"}

{block name="head_title"}Просмотр доноров, страница <?= $page ?> из <?= $totalPages ?>{/block}

{block name="body_content"}
<h2>Доноры</h2>

<!-- пагинация -->
<div class="pagination pagination-centered pagination-large">
    <ul>
		<?
		for ($i = 1; $i <= $totalPages; $i++) {
			if ($i == $page) {
				?>
                <li class="active"><a
                        href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/list/<?=$i?>/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>"><?=$i?></a>
                </li>
				<?
			} else {
				?>
                <li>
                    <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/list/<?=$i?>/<?=(!empty($_GET)) ? ("?" . http_build_query($_GET)) : ("")?>"><?=$i?></a>
                </li>
				<?
			}
		}
		?>
    </ul>
</div>

<?
if (empty($items)) {
	?>
<div class="alert alert-info">Тут пока пусто.</div>
<?
} else {
	?>
<table class="table table-hover">
    <thead>
    <tr>
		<?
		foreach ($fields as $field) {
			if (!$field->isListHidden) {
				?>
                <th><?=$field->displayName?></th>
				<?
			}
		}
		?>
        <th>Действия</th>
    </tr>
    </thead>
    <tbody>
		<?
		foreach ($items as $item) {
			?>
        <tr>
			<?
			foreach ($fields as $field) {
				if (!$field->isListHidden) {
					$field->currentValue = $field->convertDbFormatToValue($item[$field->fieldName]);
					?>
                    <td><?=$field->render("list")?></td>
					<?
				}
			}
			?>
            <td>
				<nobr>	
                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/edit/<?=$item['id']?>"><i
                        class="icon-pencil" title="редактировать"></i></a>
                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/posts/list/?donor_id=<?=intval($item['id'])?>"><i
                        class="icon-align-justify" title="смотреть посты"></i></a>
                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/regrab/<?=$item['id']?>"
                   onclick="return confirm('Все неотмодерированное содержимое будет удалено. Продолжить?');"><i
                        class="icon-fire" title="учинить реграб"></i></a>

                <a href="<?=\Nbsbbs\AF\Registry::getInstance()->config->baseUrl?>/donors/delete/<?=$item['id']?>"
                   style="margin-left: 20px"><i class="icon-remove" title="удалить"></i></a>
				</nobr>
            </td>
        </tr>
			<?
		}
		?>
    </tbody>
</table>
<?
}
?>
{/block}