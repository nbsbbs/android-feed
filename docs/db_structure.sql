-- phpMyAdmin SQL Dump
-- version 3.1.3.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Мар 25 2013 г., 12:22
-- Версия сервера: 5.0.81
-- Версия PHP: 5.3.22

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- База данных: `android_feed`
--

-- --------------------------------------------------------

--
-- Структура таблицы `af_donors`
--

CREATE TABLE IF NOT EXISTS `af_donors` (
  `id` int(11) NOT NULL auto_increment,
  `donor_title` varchar(256) NOT NULL,
  `donor_manager` int(11) NOT NULL,
  `grab_freq_h` smallint(6) NOT NULL default '24',
  `grab_error_m` smallint(6) NOT NULL default '120',
  `next_grab` int(11) NOT NULL,
  `has_categories` tinyint(4) NOT NULL default '1',
  `is_active` tinyint(4) NOT NULL default '1',
  `do_regrab` tinyint(4) NOT NULL default '0',
  `additional_fields` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Структура таблицы `af_donors_categories`
--

CREATE TABLE IF NOT EXISTS `af_donors_categories` (
  `id` int(11) NOT NULL auto_increment,
  `donor_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL default '0',
  `category_name` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `donor_id` (`donor_id`,`category_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- Структура таблицы `af_donors_posts`
--

CREATE TABLE IF NOT EXISTS `af_donors_posts` (
  `id` int(11) NOT NULL auto_increment,
  `donor_id` int(11) NOT NULL,
  `post_guid` varchar(512) NOT NULL,
  `post_title` varchar(512) NOT NULL,
  `post_text` text NOT NULL,
  `post_pubtime` int(11) NOT NULL,
  `post_grabtime` int(11) NOT NULL,
  `additional_fields` text NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `donor_id` (`donor_id`,`post_guid`(255))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=74702 ;

-- --------------------------------------------------------

--
-- Структура таблицы `af_donors_posts_categories`
--

CREATE TABLE IF NOT EXISTS `af_donors_posts_categories` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  UNIQUE KEY `post_id` (`post_id`,`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `af_grabber_managers`
--

CREATE TABLE IF NOT EXISTS `af_grabber_managers` (
  `id` int(11) NOT NULL auto_increment,
  `display_name` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;


--
-- Дамп данных таблицы `af_donors`
--

INSERT INTO `af_donors` (`id`, `donor_title`, `donor_manager`, `grab_freq_h`, `grab_error_m`, `next_grab`, `has_categories`, `is_active`, `do_regrab`, `additional_fields`) VALUES
(1, 'Bash.im', 1, 24, 60, 1364305960, 0, 1, 0, ''),
(7, 'Pornorasskaz.com', 2, 24, 120, 1364218073, 1, 0, 0, ''),
(8, 'Strashilka.com', 3, 24, 120, 1364306086, 1, 1, 0, '');

--
-- Дамп данных таблицы `af_grabber_managers`
--

INSERT INTO `af_grabber_managers` (`id`, `display_name`) VALUES
(1, 'Grabber_Donor_Bashim'),
(2, 'Grabber_Donor_Pornorasskaz'),
(3, 'Grabber_Donor_Strashilka'),
(4, 'Grabber_Donor_Statusisu'),
(5, 'Grabber_Donor_1001FactsInfo');
